package ir.vbile.app.azmoonsaz.project.feature.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseAdapter
import ir.vbile.app.azmoonsaz.framework.base.BaseViewHolder
import ir.vbile.app.azmoonsaz.project.model.data.classroom.summary.ClassroomSummary


class ClassroomSummaryAdapter(private var list: List<ClassroomSummary> = arrayListOf()) :
    BaseAdapter<ClassroomSummary, ClassroomSummaryAdapter.ClassroomViewHolder>(list as ArrayList<ClassroomSummary>) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassroomViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_classroom, parent, false)
        return ClassroomViewHolder(view)
    }

    inner class ClassroomViewHolder(item: View) : BaseViewHolder<ClassroomSummary>(item) {
        private var title: TextView
        private var participantsCount: TextView
        private var examsCount: TextView

        init {
            title = itemView.findViewById(R.id.tv_itemClassroom_title)
            participantsCount = itemView.findViewById(R.id.tv_itemClassroom_Participants_count)
            examsCount = itemView.findViewById(R.id.tv_itemClassroom_exams_count)
        }

        override fun bind(item: ClassroomSummary) {

            title.text = item.classroom.classroomName
            participantsCount.text = title.context.resources.getString(
                R.string.fragment_home_participants,
                item.classroomParticipants
            )
            examsCount.text = title.context.resources.getString(
                R.string.fragment_home_exams_count,
                item.classroomExamCount
            )
            itemView.setOnClickListener {

            }
        }

    }
}