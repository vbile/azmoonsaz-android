package ir.vbile.app.azmoonsaz.project.model.data.question

data class QuestionsResponse(
    val `question`: List<Question>,
    val pagination: Pagination
)