package ir.vbile.app.azmoonsaz


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import ir.vbile.app.azmoonsaz.databinding.ActivityMainBinding
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.core.setupWithNavController
import ir.vbile.app.azmoonsaz.framework.exception.http.UnAuthorizeException
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.framework.helper.NetworkUtil
import ir.vbile.app.azmoonsaz.project.feature.auth.OnUserAuthenticate
import ir.vbile.app.azmoonsaz.project.feature.classroom.OnClassroomUpdate
import org.greenrobot.eventbus.Subscribe

class MainActivity : BaseActivity() {


    @Subscribe
    fun handleUnAuthorizeException(unAuthorizeException: UnAuthorizeException) {
        findNavController(R.id.fcv_activityMain_main).popBackStack()
        findNavController(R.id.fcv_activityMain_main).navigate(R.id.loginFragment)
    }

    @Subscribe
    fun handleOnUserAuthenticate(onUserAuthenticate: OnUserAuthenticate) {
        findNavController(R.id.fcv_activityMain_main).popBackStack()
        findNavController(R.id.fcv_activityMain_main).navigate(R.id.homeFragment)
    }






    override fun getCoordinatorLayout(): Int = R.id.cl_activityMain_main
    private var currentNavController: LiveData<NavController>? = null
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerReceiver(broadcastReceiver, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        } // Else, need to wait for onRestoreInstanceState
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar()
    }

    /**
     * Called on first creation and when restoring state.
     */
    private fun setupBottomNavigationBar() {
        val bottomNavigationView =
            findViewById<BottomNavigationView>(binding.bnvActivityMainMain.id)

        val navGraphIds = listOf(
            R.navigation.home,
            R.navigation.classroom,
            R.navigation.statistics,
            R.navigation.profile
        )

        // Setup the bottom navigation view with a list of navigation graphs
        val controller = bottomNavigationView.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = binding.fcvActivityMainMain.id,
            intent = intent
        )

        // Whenever the selected controller changes, setup the action bar.
        controller.observe(this, Observer { navController ->
//      setupActionBarWithNavController(navController)
        })
        currentNavController = controller
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }


    fun showBottomNav() {
        binding.bnvActivityMainMain.visibility = View.VISIBLE

    }

    fun hideBottomNav() {
        binding.bnvActivityMainMain.visibility = View.GONE
    }


    val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
//            checkInternetConnection()
        }

    }

    private fun checkInternetConnection() {
        if (!NetworkUtil.isNetworkConnected(this)) {
            try {
                findNavController(R.id.fcv_activityMain_main).navigate(R.id.splashFragment)
            } catch (e: IllegalArgumentException) {
                DebugHelper.info(
                    "Multiple navigation attempts handled." + e.message
                )
            }
        } else {
            try {
                findNavController(R.id.fcv_activityMain_main).navigate(R.id.splashFragment)
            } catch (e: IllegalArgumentException) {
                DebugHelper.info(
                    "Multiple navigation attempts handled." + e.message
                )
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }
}
