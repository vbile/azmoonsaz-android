package ir.vbile.app.azmoonsaz.framework.component;

import com.alirezaafkar.sundatepicker.components.DateItem;

import java.util.Calendar;
import java.util.Locale;

public class VbileCalender {


    class Date extends DateItem {
        String getDate() {
            Calendar calendar = getCalendar();
            return String.format(Locale.US,
                    "%d/%d/%d (%d/%d/%d)",
                    getYear(), getMonth(), getDay(),
                    calendar.get(Calendar.YEAR),
                    +calendar.get(Calendar.MONTH) + 1,
                    +calendar.get(Calendar.DAY_OF_MONTH));
        }
    }
}
