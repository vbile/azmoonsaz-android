package ir.vbile.app.azmoonsaz.project.feature.splash

import android.os.Bundle
import android.text.TextUtils
import androidx.annotation.IdRes
import androidx.core.os.bundleOf
import androidx.navigation.*
import androidx.navigation.fragment.findNavController
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import ir.vbile.app.azmoonsaz.framework.core.App
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.framework.helper.NetworkUtil
import ir.vbile.app.azmoonsaz.project.feature.auth.TokenContainer

class SplashFragment : ObserverFragment() {

    override val getLayoutRes: Int
        get() = R.layout.fragment_splash

    override fun setupViews() {
        hideBottomNavigationView()
        DebugHelper.info("Internet => " + NetworkUtil.isNetworkConnected(requireContext()))
        App.handler.postDelayed({
            if (!NetworkUtil.isNetworkConnected(requireContext())) {
                // ToDo show internet setting and wifi setting if network failed
                // (activity as BaseActivity).showToast(L.network_connection_failed)
                findNavController().navigate(R.id.action_splashFragment_to_networkFailedFragment)
                DebugHelper.info("No Internet")
            } else if (TextUtils.isEmpty(TokenContainer.token)) {
                DebugHelper.info("Token Invalid")
                findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
            } else {
                DebugHelper.info("Go To Home")
                findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
            }
        }, 500)
    }


    override fun subscribe() {

    }
}