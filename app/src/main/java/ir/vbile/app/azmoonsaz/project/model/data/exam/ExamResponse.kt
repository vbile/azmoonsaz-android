package ir.vbile.app.azmoonsaz.project.model.data.exam


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class ExamResponse(
    @SerializedName("data")
    var `data`: List<Exam>,
    @SerializedName("pagination")
    var pagination: Pagination
) : Parcelable