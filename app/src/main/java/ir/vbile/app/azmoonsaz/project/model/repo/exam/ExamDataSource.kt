package ir.vbile.app.azmoonsaz.project.model.repo.exam

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.classroom.ClassroomResponse
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam
import ir.vbile.app.azmoonsaz.project.model.data.exam.ExamResponse
import ir.vbile.app.azmoonsaz.project.model.data.exam.one.ExamUpdateResponse
import ir.vbile.app.azmoonsaz.project.model.repo.DataSource

interface ExamDataSource {
    fun get(id: Int): Single<ExamResponse>
    fun delete(item: ExamResponse): Completable
    fun getAll(page: Int): Single<List<ExamResponse>>
    fun insert(item: ExamResponse): Single<ExamResponse>
    fun update(classroomId: Int, examId: Int, jsonObject: JsonObject): Single<ExamUpdateResponse>
    fun getAllExams(): Flowable<ExamResponse>

}