package ir.vbile.app.azmoonsaz.project.feature.classroom

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import io.reactivex.disposables.CompositeDisposable
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom

class CreateClassroomDialog : DialogFragment() {
    private lateinit var mainViewModel: ClassroomViewModel
    private val disposable = CompositeDisposable()

    private lateinit var eventListener: CreateClassroomDialogEventListener

    companion object {
        fun newInstance(
            eventListener: CreateClassroomDialogEventListener,
            mainViewModel: ClassroomViewModel
        ): CreateClassroomDialog =
            CreateClassroomDialog()
                .apply {
                    this.eventListener = eventListener
                    this.mainViewModel = mainViewModel
                    /*
                    arguments = Bundle().apply {
                        putInt(ARG_ITEM_COUNT, itemCount)
                    }
                     */
                }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(requireContext())
            .inflate(R.layout.dialog_classroom, null, false)
        val btnClose = view.findViewById<FrameLayout>(R.id.btn_dialogClassroom_dismiss)
        val btnCreate = view.findViewById<MaterialButton>(R.id.btn_dialogClassroom)
        val edtClassroomName =
            view.findViewById<TextInputEditText>(R.id.edt_dialogClass_classroomName)
        btnClose.setOnClickListener {
            dismiss()
        }

        btnCreate.setOnClickListener {
            val classroomName = edtClassroomName.text.toString().trim()
            val classroom = Classroom(classroomName)
            (activity as BaseActivity).showToast(classroomName)
            eventListener.onBtnCreateClicked(classroom)
        }
        val builder = MaterialAlertDialogBuilder(requireContext())
        builder.setView(view)
        return builder.create()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog!!.window!!.getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    interface CreateClassroomDialogEventListener {
        fun onBtnCreateClicked(classroom: Classroom)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        disposable.clear()
    }
}