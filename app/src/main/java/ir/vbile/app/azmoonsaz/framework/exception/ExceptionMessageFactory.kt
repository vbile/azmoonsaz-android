package ir.vbile.app.azmoonsaz.framework.exception

import com.google.gson.Gson
import io.reactivex.exceptions.UndeliverableException
import ir.vbile.app.azmoonsaz.framework.exception.http.UnAuthorizeException
import ir.vbile.app.azmoonsaz.framework.exception.http.e401.AuthorizationException
import ir.vbile.app.azmoonsaz.framework.exception.http.e422.ServerValidationException
import ir.vbile.app.azmoonsaz.project.model.data.auth.ServerResponse
import org.greenrobot.eventbus.EventBus
import retrofit2.HttpException
import java.io.IOException

object ExceptionMessageFactory {

    fun getMessage(throwable: Throwable): String? {
        if (throwable is HttpException) {
            when (throwable.code()) {
                201 -> {
                    val gson = Gson()
                    try {
                        val exception = gson.fromJson(
                            throwable
                                .response()
                                ?.errorBody()?.string(),
                            ServerResponse::class.java
                        )
                        return exception.message
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                401 -> {
                    EventBus.getDefault().post(UnAuthorizeException())
                    val gson = Gson()
                    try {
                        val exception = gson.fromJson(
                            throwable
                                .response()
                                ?.errorBody()?.string(),
                            AuthorizationException::class.java
                        )
                        return exception.message
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                403 -> {
                    EventBus.getDefault().post(UnAuthorizeException())
                }
                404
                -> {
                    val gson = Gson()
                    try {
                        val exception = gson.fromJson(
                            throwable
                                .response()
                                ?.errorBody()?.string(),
                            ServerResponse::class.java
                        )
                        return exception.message
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                422 -> {
                    val gson = Gson()
                    try {
                        val exception = gson.fromJson(
                            throwable
                                .response()
                                ?.errorBody()?.string(),
                            ServerValidationException::class.java
                        )
                        if (exception.errors.mobile != null) {
                            return exception.errors.mobile[0]
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }

        if (throwable is UndeliverableException) {
            return throwable.message.toString()
        }

        return throwable.message
    }
}