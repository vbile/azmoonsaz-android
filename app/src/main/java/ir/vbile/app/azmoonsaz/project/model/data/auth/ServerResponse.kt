package ir.vbile.app.azmoonsaz.project.model.data.auth


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class ServerResponse(
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: Int
) : Parcelable