package ir.vbile.app.azmoonsaz.project.feature.classroom

import android.content.Context
import android.view.WindowManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.base.OnRvItemClickListener
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.core.getActivity
import ir.vbile.app.azmoonsaz.framework.handler.SingleVbileObserver
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.project.feature.adapters.ClassroomAdapter
import ir.vbile.app.azmoonsaz.project.feature.adapters.ClassroomSummaryAdapter
import ir.vbile.app.azmoonsaz.project.feature.auth.OnUserAuthenticate
import ir.vbile.app.azmoonsaz.project.feature.exam.ExamDialog
import ir.vbile.app.azmoonsaz.project.feature.exam.MaterialDialog
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.classroom.ClassroomResponse
import ir.vbile.app.azmoonsaz.project.model.data.classroom.summary.ClassroomSummaryResponse
import ir.vbile.app.azmoonsaz.project.model.data.classroom.update.ClassroomUpdateResponse
import kotlinx.android.synthetic.main.fragment_classroom.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.koin.android.ext.android.inject

class ClassroomFragment : ObserverFragment(),
    CreateClassroomDialog.CreateClassroomDialogEventListener,
    ClassroomAdapter.ClassroomEventListener {
    private val classroomViewModel by inject<ClassroomViewModel>()
    private lateinit var classroomAdapter: ClassroomAdapter
    private lateinit var classroomSummaryAdapter: ClassroomSummaryAdapter
    private var itemOnEditPosition: Int = -1
    private var itemOnDeletePosition: Int = -1
    private lateinit var deleteDialog: MaterialDialog
    private lateinit var classroomDialog: ClassroomDialog
    override fun subscribe() {
        classroomAdapter.setOnRvItemClickListener(object : OnRvItemClickListener<Classroom> {
            override fun onItemClick(item: Classroom, position: Int) {
                val action =
                    ClassroomFragmentDirections.actionClassroomFragmentToExamFragment2(item)
                findNavController().navigate(action)
            }
        })
        classroomAdapter.setOnClassroomItemEventListener(this)
        getClassroom()
//        getClassroomSummary()

    }

    private fun getClassroom() {
        classroomViewModel.getClassrooms()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<ClassroomResponse>(getActivity) {
                override fun onSuccess(classroomResponse: ClassroomResponse) {
                    DebugHelper.info("classroomSummaryResponse => " + classroomResponse.data.size)
                    if (classroomResponse.data.size > 0) {
                        classroomAdapter.addAll(classroomResponse.data)
                        rv_fragmentClassroom_classrooms.layoutManager =
                            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                        rv_fragmentClassroom_classrooms.adapter = classroomAdapter
                    }
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }
            })
    }

    override val getLayoutRes: Int = R.layout.fragment_classroom

    override fun setupViews() {
        btn_fragmentClassroom_create_class.setOnClickListener {
            classroomDialog = ClassroomDialog(
                ClassroomDialog.MODE_CREATE,
                onClicked = {
                    createClassroom(this)
                })
            classroomDialog.isCancelable = false
            classroomDialog.show(getActivity.supportFragmentManager, null)
        }
        classroomAdapter = ClassroomAdapter()
    }

    override fun onBtnCreateClicked(classroom: Classroom) {

    }


    override fun onEdit(classroom: Classroom, position: Int) {
        itemOnEditPosition = position
        classroomDialog = ClassroomDialog(
            ClassroomDialog.MODE_EDIT,
            onClicked = {
                updateClassroom(classroom, this)
            })
        classroomDialog.initClassroom(classroom)
        classroomDialog.isCancelable = false
        classroomDialog.show(getActivity.supportFragmentManager, null)
    }

    override fun onDelete(classroom: Classroom, position: Int) {
        itemOnDeletePosition = position
        deleteDialog = MaterialDialog(
            L.classroom_delete_title,
            L.classroom_delete_message,
            onYes = {
                deleteClass(classroom)
                deleteDialog.dismiss()
            },
            onNo = {
                deleteDialog.dismiss()
            }
        )
        deleteDialog.isCancelable = false
        deleteDialog.show(getActivity.supportFragmentManager, null)
    }

    private fun deleteClass(classroom: Classroom) {

    }

    private fun updateClassroom(classroom: Classroom, classroomName: String) {
        classroomViewModel.update(classroom, classroomName)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnEvent { t1, t2 ->
                classroomDialog.dismiss()
            }
            .subscribe(object : SingleVbileObserver<Classroom>(getActivity) {
                override fun onSuccess(classroomResponse: Classroom) {
                    DebugHelper.info(classroomResponse.name + "updated")
                    getActivity.showToast(classroomResponse.name + "updated")
                    classroomAdapter.updateItem(itemOnEditPosition, classroomResponse)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }
            })

    }

    private fun createClassroom(classroomName: String) {
        classroomViewModel.createClassroom(classroomName)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<Classroom>(activity as BaseActivity) {
                override fun onSuccess(classroom: Classroom) {
                    classroomAdapter.addItem(classroom)
                    classroomDialog.dismiss()
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }

    private fun getClassroomSummary() {
        classroomViewModel.getClassroomSummary()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<ClassroomSummaryResponse>(getActivity) {
                override fun onSuccess(classroomSummaryResponse: ClassroomSummaryResponse) {
                    DebugHelper.info("classroomSummaryResponse => " + classroomSummaryResponse.classroomSummary.size)
                    if (classroomSummaryResponse.classroomSummary.size > 0) {
                        classroomSummaryAdapter =
                            ClassroomSummaryAdapter(classroomSummaryResponse.classroomSummary)
                        rv_fragmentClassroom_classrooms.layoutManager =
                            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                        rv_fragmentClassroom_classrooms.adapter = classroomSummaryAdapter
                    }
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }
            })
    }
    @Subscribe
    fun handleOnClassroomUpdate(onClassroomUpdate: OnClassroomUpdate) {
        getClassroom()
        DebugHelper.info("handleOnClassroomUpdate")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }
}