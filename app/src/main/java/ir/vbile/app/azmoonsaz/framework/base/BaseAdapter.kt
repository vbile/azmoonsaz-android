package ir.vbile.app.azmoonsaz.framework.base


import androidx.recyclerview.widget.RecyclerView
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam


abstract class BaseAdapter<T, E : BaseViewHolder<T>> : RecyclerView.Adapter<E> {

    private var onRvItemClickListener: OnRvItemClickListener<T>? = null
    private var items = arrayListOf<T>()

    constructor()
    constructor(items: ArrayList<T>) {
        this.items = items

    }

    constructor(onRvItemClickListener: OnRvItemClickListener<T>?, items: ArrayList<T>) : super() {
        this.items = items
        this.onRvItemClickListener = onRvItemClickListener
    }


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: E, position: Int) {
        holder.bind(items[position])
        if (onRvItemClickListener != null) {
            holder.itemView.setOnClickListener {

                onRvItemClickListener!!.onItemClick(items[position], position)
            }
        }
    }

    fun addItem(item: T) {
        items.add(0, item)
        notifyItemChanged(0)
    }

    fun setOnRvItemClickListener(onRvItemClickListener: OnRvItemClickListener<T>) {
        this.onRvItemClickListener = onRvItemClickListener
    }


    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    // Add a list of items -- change to type used
    fun addAll(list: List<T>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, items.size)
    }

    fun restoreItem(item: T, position: Int) {
        items.add(position, item)
        // notify item added by position
        notifyItemInserted(position)
    }

    fun getData(): List<T> {
        return items
    }

    fun updateItem(position: Int, item: T) {
        items[position] = item
        notifyItemChanged(position)
    }

}