package ir.vbile.app.azmoonsaz.project.model.data.classroom.summary


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Classroom(
    @SerializedName("id")
    var classroomId: Int,
    @SerializedName("name")
    var classroomName: String,
    @SerializedName("user_name")
    var userName: String
) : Parcelable