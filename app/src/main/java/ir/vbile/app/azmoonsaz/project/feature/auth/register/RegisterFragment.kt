package ir.vbile.app.azmoonsaz.project.feature.auth.register

import androidx.navigation.fragment.findNavController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.handler.SingleVbileObserver
import ir.vbile.app.azmoonsaz.framework.helper.MyPasswordTransformationMethod
import ir.vbile.app.azmoonsaz.project.feature.auth.AuthenticationViewModel
import ir.vbile.app.azmoonsaz.project.model.data.auth.RegisterResponse
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_register.*
import org.koin.android.ext.android.inject

class RegisterFragment : ObserverFragment() {

    private val authenticationViewModel by inject<AuthenticationViewModel>()

    override val getLayoutRes: Int get() = R.layout.fragment_register


    override fun setupViews() {
        hideBottomNavigationView()
        btn_fragmentRegister_register.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_confirmCodeFragment)
        }
        tv_fragmentRegister_alreadyHaveAccount.setOnClickListener {
            findNavController().popBackStack()
        }

        edt_fragmentRegister_password.transformationMethod = MyPasswordTransformationMethod()
        edt_fragmentRegister_confirm_password.transformationMethod =
            MyPasswordTransformationMethod()

    }

    override fun subscribe() {

        btn_fragmentRegister_register.setOnClickListener {
            registerUser()
        }
    }

    private fun registerUser() {
        val name = edt_fragmentRegister_name.text.toString().trim()
        val family = edt_fragmentRegister_family.text.toString().trim()
        val password = edt_fragmentRegister_password.text.toString().trim()
        val passwordConfirm = edt_fragmentRegister_confirm_password.text.toString().trim()
        val mobile = edt_fragmentRegister_mobile.text.toString().trim()
        if (validateRegister(name, family, password, passwordConfirm, mobile)) {
            sendRegisterUserRequest(name, family, password, passwordConfirm, mobile)
        }
    }

    private fun sendRegisterUserRequest(
        name: String,
        family: String,
        password: String,
        passwordConfirm: String,
        mobile: String
    ) {
        authenticationViewModel.register(name, family, mobile, password)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<RegisterResponse>(activity as BaseActivity) {
                override fun onSuccess(registerResponse: RegisterResponse) {
                    if (registerResponse.status == 201) {
                        val action =
                            RegisterFragmentDirections.actionRegisterFragmentToConfirmCodeFragment(
                                mobile,
                                password
                            )
                        findNavController().navigate(action)
                    } else if (registerResponse.status == 403) {
                        (activity as BaseActivity).showSnackBar(L.your_account_is_not_active)
                    }
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }

    private fun validateRegister(
        name: String,
        family: String,
        password: String,
        passwordConfirm: String,
        mobile: String
    ): Boolean {
        return true
    }

}