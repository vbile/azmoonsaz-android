package ir.vbile.app.azmoonsaz.project.feature.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.button.MaterialButton
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseAdapter
import ir.vbile.app.azmoonsaz.framework.base.BaseViewHolder
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam


class ClassroomAdapter(private var list: List<Classroom> = arrayListOf()) :
    BaseAdapter<Classroom, ClassroomAdapter.ClassroomViewHolder>(list as ArrayList<Classroom>) {

    private var classroomEventListener: ClassroomEventListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassroomViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_classroom, parent, false)
        return ClassroomViewHolder(view)
    }

    inner class ClassroomViewHolder(item: View) : BaseViewHolder<Classroom>(item) {
        private var title: TextView
        private var examsCount: TextView
        private var btnDelete: MaterialButton
        private var btnEdit: MaterialButton

        init {
            title = itemView.findViewById(R.id.tv_itemClassroom_title)
            examsCount = itemView.findViewById(R.id.tv_itemClassroom_exams_count)
            btnDelete = itemView.findViewById(R.id.btn_itemClass_delete)
            btnEdit = itemView.findViewById(R.id.btn_itemClass_edit)
        }

        override fun bind(item: Classroom) {
            title.text = item.name
            examsCount.text = if (item.examsCount != null && item.examsCount!! > 0)
                L.exam_count(item.examsCount!!)
            else
                L.exam_not_found
            btnDelete.setOnClickListener {
                classroomEventListener?.onDelete(item, adapterPosition)
            }
            btnEdit.setOnClickListener {
                classroomEventListener?.onEdit(item, adapterPosition)
            }
        }
    }

    interface ClassroomEventListener {
        fun onEdit(classroom: Classroom, position: Int)
        fun onDelete(classroom: Classroom, position: Int)
    }

    fun setOnClassroomItemEventListener(classroomEventListener: ClassroomEventListener) {
        this.classroomEventListener = classroomEventListener
    }

}