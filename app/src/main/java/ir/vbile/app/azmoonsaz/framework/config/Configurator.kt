package ir.vbile.app.azmoonsaz.framework.config

interface Configurator {
    fun config()
    fun populateNotificationChannels()
    // And your custom config functions
}