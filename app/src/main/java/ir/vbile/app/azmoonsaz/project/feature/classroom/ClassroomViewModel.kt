package ir.vbile.app.azmoonsaz.project.feature.classroom

import com.google.gson.JsonObject
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.classroom.ClassroomResponse
import ir.vbile.app.azmoonsaz.project.model.repo.classroom.ClassroomRepository
import ir.vbile.app.azmoonsaz.project.model.data.classroom.summary.ClassroomSummaryResponse
import ir.vbile.app.azmoonsaz.project.model.data.classroom.update.ClassroomUpdateResponse

class ClassroomViewModel(private val classroomRepository: ClassroomRepository) {

    fun createClassroom(classroomName: String): Single<Classroom> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", classroomName)
        return classroomRepository.createClassroom(jsonObject)
    }

    fun getClassrooms(): Single<ClassroomResponse> {
        return classroomRepository.getClassrooms(1)
    }

    fun getClassroomSummary(): Single<ClassroomSummaryResponse> {
        return classroomRepository.getClassroomSummary()
    }

    fun update(classroom: Classroom, classroomName: String): Single<Classroom> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", classroomName)
        return classroomRepository.update(classroom.id!!, jsonObject)
    }

}