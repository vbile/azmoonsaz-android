package ir.vbile.app.azmoonsaz.project.model.data.classroom.summary


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class ClassroomSummaryResponse(
    @SerializedName("classroom_summary")
    var classroomSummary: List<ClassroomSummary>,
    @SerializedName("meta")
    var meta: Meta
) : Parcelable