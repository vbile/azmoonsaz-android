package ir.vbile.app.azmoonsaz.framework.helper

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.vbile.app.azmoonsaz.R

class DialogBottom : BottomSheetDialogFragment() {
    var onBtnClicked: OnBtnClicked? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        onBtnClicked = context as OnBtnClicked
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.dialog_classroom, null, false)
    }

    interface OnBtnClicked {
        fun onClicked(classroom: String?): String?
    }
}