package ir.vbile.app.azmoonsaz.framework.config.lang

abstract class Lang {

    open val exam_edit: String = "Edit Exam"
    open val your_account_is_not_active: String = "Your account is not active"
    open val welcome: String = "welcome"
    open val check_your_phone_number: String = "Please Enter Your Phone Number"
    open val check_your_password: String = "Please Enter Your Password"
    open val wrong_phone_number: String = "Please Enter Correct PhoneNumber"

    open val you_are_not_register_yet: String = "You are nt registered"
    open val unauthorized: String = "unauthorized"
    open val wrong_password: String = "Please Enter Correct Password"

    open val network_connection_failed: String = "Tou haven't internet check your network"
    open val menu_about_application = "About Application"
    open val menu_application_category = "Application"
    open val menu_about = "About"
    open val menu_feedback = "Feedback"
    open val menu_preferences = "Preferences"
    open val menu_products = "Products"
    open val notification_title = "Hello from notification module"
    open val notification_text = "Some Notification"
    open val notification_id = "notification_id"
    open val button_yes = "yes"
    open val button_no = "no"
    open val button_cancel = "CANCEL"
    open val button_ok = "OK"
    open val hello_world = "hello"
    open val permission_required_message =
        "Please grant all permission granted. \n If not, app wont' work normally"
    open val permission_required_title = "Permission Required"
    open val appName = "Renano App"
    open val version = "version: "


    // Exam Lang Resource
    open fun exam_count(count: Int): String = "Exam count: $count"
    open val exam_not_found: String = "This classroom don't have any statistics, please add som statistics"
    val statusArray = listOf("draft", "published", "disabled")
    open fun getExamStatus(): List<String> = statusArray
    open fun getTimes(): List<String> {
        val times = mutableListOf<String>()
        for (i in 1 until 59) {
            times.add("$i minute")
        }
        return times
    }

    open fun getExamsStudentsCount(studentsCount: Int?): String {
        return "Students Count: $studentsCount"
    }

    open fun getExamStartDate(date: String): String {
        return "Start Date: $date"
    }

    open fun getExamEndDate(date: String): String {
        return "End Date: $date"
    }

    open fun getExamDuration(examDuration: String): String {
        return "Duration: $examDuration"
    }

    fun getEnVersionOfStatus(status: String): String {
        if (status == "پیش نویس") {
            return "draft"
        } else if (status.equals("منتشر شده")) {
            return "published"
        } else
            return "disabled"
    }


    // Exam
    open var exam_delete_message: String = "Are you sure the statistics will be deleted?"
    open var exam_deleted_successfully: String = "Exam successfully deleted"
    open var exam_delete_title: String = "Delete the test"
    open val exam_not_exist: String = "There aren't any statistics"
    open val exam_end_date: String = "End Date:"
    open val exam_start_date: String = "Start Date:"


    // Classroom
    open var classroom_delete_message: String = "Are you sure the classroom will be deleted?"
    open var classroom_deleted_successfully: String = "Classroom successfully deleted"
    open var classroom_delete_title: String = "Delete the class"
    open var classroom_not_exist: String = "There aren't any classroom"
    open var classroom_edit: String = "Edit Classroom"
    open var classroom_create: String = "Create Classroom"
}