package ir.vbile.ap

import io.reactivex.disposables.Disposable
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import androidx.navigation.fragment.navArgs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.handler.CompletableVbileObserver
import ir.vbile.app.azmoonsaz.framework.handler.SingleVbileObserver
import ir.vbile.app.azmoonsaz.project.feature.auth.AuthenticationViewModel
import ir.vbile.app.azmoonsaz.project.feature.auth.OnUserAuthenticate
import ir.vbile.app.azmoonsaz.project.model.data.auth.ServerResponse
import kotlinx.android.synthetic.main.fragment_confirm_code.*
import org.greenrobot.eventbus.EventBus
import org.koin.android.ext.android.inject

class ConfirmCodeFragment : ObserverFragment() {
    private val authenticationViewModel by inject<AuthenticationViewModel>()
    val args: ConfirmCodeFragmentArgs by navArgs()

    override val getLayoutRes: Int
        get() = R.layout.fragment_confirm_code


    override fun subscribe() {
        hideBottomNavigationView()
        DebugHelper.info("")
        otp_fragmentConfirmCode_code.setOtpCompletionListener {
            sendConfirmRequest(it)
        }
        btn_fragmentConfirmCode_login.setOnClickListener {
            val otp = otp_fragmentConfirmCode_code.text.toString().trim()
            sendConfirmRequest(otp)
        }

    }

    private fun sendConfirmRequest(otp: String) {
        authenticationViewModel.confirm(args.mobile, args.password, otp)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<ServerResponse>(activity as BaseActivity) {
                override fun onSuccess(serverResponse: ServerResponse) {
                    DebugHelper.info(serverResponse.message)
                    if (serverResponse.status == 200) {
                        authenticationViewModel.authenticate(
                            requireContext(),
                            args.mobile,
                            args.password
                        )
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.newThread())
                            .subscribe(object :
                                CompletableVbileObserver(activity as BaseActivity) {
                                override fun onComplete() {
                                    EventBus.getDefault().post(OnUserAuthenticate())
                                    (activity as BaseActivity).showToast(L.welcome)
                                }

                                override fun onSubscribe(d: Disposable) {
                                    compositeDisposable.add(d)
                                }

                            })
                    }
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onError(e: Throwable) {
                    super.onError(e)
                    e.message?.let { it1 -> DebugHelper.info(it1) }

                }
            })
    }


    override fun setupViews() {

    }
}