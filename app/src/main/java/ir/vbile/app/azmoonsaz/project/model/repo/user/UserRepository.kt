package ir.vbile.app.azmoonsaz.project.model.repo.user

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.auth.*
import ir.vbile.app.azmoonsaz.project.model.repo.DataSource

class UserRepository(private val userCloudDataSource: UserCloudDataSource) : DataSource<User, Int> {
    override fun get(id: Int): Single<User> {
        TODO("Not yet implemented")
    }

    override fun delete(item: User): Completable {
        TODO("Not yet implemented")
    }

    override fun getAll(page: Int): Single<List<User>> {
        TODO("Not yet implemented")
    }

    override fun insert(item: User): Single<User> {
        TODO("Not yet implemented")
    }

    override fun update(item: User): Single<User> {
        TODO("Not yet implemented")
    }

    fun getUer(): Single<User> {
        TODO("Not yet implemented")
    }

    fun register(jsonObject: JsonObject): Single<RegisterResponse> {
        return userCloudDataSource.register(jsonObject)
    }

    fun confirm(jsonObject: JsonObject): Single<ServerResponse> {
        return userCloudDataSource.confirm(jsonObject)
    }

    fun login(jsonObject: JsonObject): Single<LoginResponse> {
        return userCloudDataSource.login(jsonObject)
    }

    fun getToken(
        grant_type: String,
        client_id: Int,
        client_secret: String,
        mobile: String,
        password: String
    ): Single<Token> {
        return  userCloudDataSource.getToken(
            grant_type,
            client_id,
            client_secret,
            mobile,
            password)
    }
}