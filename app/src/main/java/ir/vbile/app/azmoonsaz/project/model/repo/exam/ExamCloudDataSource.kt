package ir.vbile.app.azmoonsaz.project.model.repo.exam

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.core.service.network.ApiService
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam
import ir.vbile.app.azmoonsaz.project.model.data.exam.ExamResponse
import ir.vbile.app.azmoonsaz.project.model.data.exam.one.ExamUpdateResponse
import java.lang.Exception

class ExamCloudDataSource(private val apiService: ApiService) {
    fun get(id: Int): Single<ExamResponse> {
        TODO("Not yet implemented")
    }

    fun delete(exam: Exam): Completable {
        return apiService.deleteExam(exam.examClass?.id!!, exam.examId!!)
    }

    fun getAll(page: Int): Single<List<ExamResponse>> {
        TODO("Not yet implemented")
    }

    fun insert(classroomId: Int, jsonObject: JsonObject): Single<Exam> {
        return apiService.insertExam(classroomId, jsonObject)
            .flatMap {
                return@flatMap Single.create<Exam>({ emitter ->
                    try {
                        if (!emitter.isDisposed) {
                            emitter.onSuccess(it.exam)
                        }
                    } catch (exception: Exception) {
                        if (!emitter.isDisposed) {
                            emitter.onError(exception)
                        }
                    }

                })
            }
    }

    fun update(
        classroomId: Int,
        examId: Int,
        jsonObject: JsonObject
    ): Single<ExamUpdateResponse> {
        return apiService.updateExam(classroomId, examId, jsonObject)
    }


    fun getAllExams(): Flowable<ExamResponse> {
        return apiService.getAllExams()
    }

    fun getExamsByClassId(classId: Int): Flowable<ExamResponse> {
        return apiService.getExamsByClassId(classId)
    }
}
