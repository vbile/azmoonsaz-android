package ir.vbile.app.azmoonsaz.project.model.data.auth


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import kotlinx.android.parcel.RawValue

@SuppressLint("ParcelCreator")
@Parcelize
data class User(
    @SerializedName("activated_at")
    var activatedAt: String,
    @SerializedName("activation_code")
    var activationCode: @RawValue Any,
    @SerializedName("created_at")
    var createdAt: String,
    @SerializedName("deleted_at")
    var deletedAt: @RawValue Any,
    @SerializedName("email")
    var email: @RawValue Any,
    @SerializedName("family")
    var family: @RawValue Any,
    @SerializedName("id")
    var id: Int,
    @SerializedName("mobile")
    var mobile: String,
    @SerializedName("name")
    var name: @RawValue Any,
    @SerializedName("updated_at")
    var updatedAt: String
) : Parcelable