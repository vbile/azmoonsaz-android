package ir.vbile.app.azmoonsaz.project.core.service.network

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.auth.LoginResponse
import ir.vbile.app.azmoonsaz.project.model.data.auth.RegisterResponse
import ir.vbile.app.azmoonsaz.project.model.data.auth.ServerResponse
import ir.vbile.app.azmoonsaz.project.model.data.auth.Token
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.classroom.ClassroomResponse
import ir.vbile.app.azmoonsaz.project.model.data.classroom.summary.ClassroomSummaryResponse
import ir.vbile.app.azmoonsaz.project.model.data.classroom.update.ClassroomUpdateResponse
import ir.vbile.app.azmoonsaz.project.model.data.exam.ExamResponse
import ir.vbile.app.azmoonsaz.project.model.data.exam.one.ExamUpdateResponse
import ir.vbile.app.azmoonsaz.project.model.data.question.QuestionsResponse
import ir.vbile.app.azmoonsaz.project.model.data.question.one.QuestionUpdateResponse
import retrofit2.http.*

interface ApiService {

    // Authentication Requests
    @POST(EndPoints.REGISTER_URL)
    fun register(@Body jsonObject: JsonObject): Single<RegisterResponse>

    @POST(EndPoints.CONFIRM_URL)
    fun confirm(@Body jsonObject: JsonObject): Single<ServerResponse>

    @POST(EndPoints.LOGIN_URL)
    fun login(@Body jsonObject: JsonObject): Single<LoginResponse>

    @FormUrlEncoded
    @POST(EndPoints.TOKEN_URL)
    fun getToken(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: Int,
        @Field("client_secret") clientSecret: String,
        @Field("username") mobile: String,
        @Field("password") password: String
    ): Single<Token>


    // Classroom URLs
    @POST(EndPoints.CREATE_CLASSROOM)
    fun createClassroom(@Body jsonObject: JsonObject): Single<Classroom>

    @GET(EndPoints.GET_ALL_CLASSROOM)
    fun getClassrooms(@Query("page") page: Int): Single<ClassroomResponse>

    @GET(EndPoints.GET_CLASSROOM_SUMMARY)
    fun getClassroomSummary(): Single<ClassroomSummaryResponse>


    // Exam URLs
    @GET(EndPoints.GET_ALL_EXAMS)
    fun getAllExams(): Flowable<ExamResponse>

    @PUT(EndPoints.UPDATE_EXAM)
    fun updateExam(
        @Path("classroom") classroom: Int,
        @Path("statistics") exam: Int,
        @Body jsonObject: JsonObject
    ): Single<ExamUpdateResponse>

    @DELETE(EndPoints.DELETE_EXAM)
    fun deleteExam(
        @Path("classroom") classroom: Int,
        @Path("statistics") exam: Int
    ): Completable

    @GET(EndPoints.GET_EXAMS_BY_CLASSROOM_ID)
    fun getExamsByClassId(
        @Path("classroom") classroom: Int
    ): Flowable<ExamResponse>

    @PUT(EndPoints.UPDATE_CLASSROOM)
    fun updateClassroom(
        @Path("classroom") classroom: Int,
        @Body jsonObject: JsonObject
    ): Single<ClassroomUpdateResponse>

    @POST(EndPoints.INSERT_EXAM)
    fun insertExam(
        @Path("classroom") classroom: Int,
        @Body jsonObject: JsonObject
    ): Single<ExamUpdateResponse>


    //Questions

    @GET(EndPoints.GET_ALL_QUESTIONS)
    fun getAllQuestions(
        @Path("classroom") classroom: Int,
        @Path("exams") examId: Int
    ): Flowable<QuestionsResponse>


    @POST(EndPoints.INSERT_QUESTION)
    fun insertQuestion(
        @Path("classroom") classroom: Int,
        @Path("exams") examId: Int,
        @Body jsonObject: JsonObject
    ): Single<QuestionUpdateResponse>


    @PUT(EndPoints.UPDATE_QUESTIONS)
    fun updateQuestion(
        @Path("classroom") classroom: Int,
        @Path("exams") examId: Int,
        @Path("questions") questionId: Int,
        @Body jsonObject: JsonObject
    ): Single<QuestionUpdateResponse>

    @DELETE(EndPoints.DELETE_QUESTIONS)
    fun deleteQuestion(
        @Path("classroom") classroom: Int,
        @Path("exams") examId: Int,
        @Path("questions") questionId: Int
    ): Completable


}