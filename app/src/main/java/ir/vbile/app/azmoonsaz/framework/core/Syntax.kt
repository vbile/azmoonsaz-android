package ir.vbile.app.azmoonsaz.framework.core

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.base.BaseFragment

typealias MethodBlock<T> = T.() -> Unit
typealias FunctionBlock = () -> Unit

typealias AndroidPermission = android.Manifest.permission

inline val Context.defaultSharedPreferences: SharedPreferences
    get() = PreferenceManager.getDefaultSharedPreferences(this)

inline val BaseFragment.getActivity: BaseActivity get() = (activity as BaseActivity)