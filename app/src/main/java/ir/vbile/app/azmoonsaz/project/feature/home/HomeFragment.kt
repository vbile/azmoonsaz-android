package ir.vbile.app.azmoonsaz.project.feature.home

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.core.getActivity
import ir.vbile.app.azmoonsaz.framework.handler.SingleVbileObserver
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.project.feature.classroom.CreateClassroomDialog
import ir.vbile.app.azmoonsaz.project.feature.adapters.ClassroomSummaryAdapter
import ir.vbile.app.azmoonsaz.project.feature.classroom.ClassroomViewModel
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.classroom.summary.ClassroomSummaryResponse
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject

class HomeFragment : ObserverFragment(), CreateClassroomDialog.CreateClassroomDialogEventListener {

    private lateinit var classroomSummaryAdapter: ClassroomSummaryAdapter
    private lateinit var fragment: BottomSheetDialogFragment
    private val mainViewModel by inject<MainViewModel>()
    private val classroomViewModel by inject<ClassroomViewModel>()
    override val getLayoutRes: Int = R.layout.fragment_home

    override fun subscribe() {
        getClassroomSummary()
    }

    private fun getClassroomSummary() {
        classroomViewModel.getClassroomSummary()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<ClassroomSummaryResponse>(getActivity) {
                override fun onSuccess(classroomSummaryResponse: ClassroomSummaryResponse) {
                    DebugHelper.info("classroomSummaryResponse => " + classroomSummaryResponse.classroomSummary.size)
                    if (classroomSummaryResponse.classroomSummary.size > 0) {
                        classroomSummaryAdapter =
                            ClassroomSummaryAdapter(classroomSummaryResponse.classroomSummary)
                        rv_fragmentHome_classrooms.layoutManager =
                            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                        rv_fragmentHome_classrooms.adapter = classroomSummaryAdapter
                        tv_fragmentSplash_exams_count.text =
                            resources.getString(
                                R.string.fragment_home_diagram_exams_count,
                                classroomSummaryResponse.meta.totalExams
                            )
                        tv_fragmentSplash_participants_count.text =
                            resources.getString(
                                R.string.fragment_home_diagram_participants_count,
                                classroomSummaryResponse.meta.totalParticipants
                            )
                    }
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }
            })
    }


    override fun setupViews() {
        showBottomNavigationView()
//        btn_fragmentHome_create_class.setOnClickListener {
//            fragment = CreateClassroomDialog.newInstance(this, mainViewModel)
//            fragment.isCancelable = false
//            getActivity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//            fragment.show(getActivity.supportFragmentManager, null)
//        }
    }

    override fun onBtnCreateClicked(classroom: Classroom) {
        classroomViewModel.createClassroom(classroom.name)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<Classroom>(activity as BaseActivity) {
                override fun onSuccess(classroom: Classroom) {
                    fragment.dismiss()
//                    classroomSummaryAdapter.addItem(classroom)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }

}