package ir.vbile.app.azmoonsaz.project.model.repo

import io.reactivex.Completable
import io.reactivex.Single

interface DataSource<T, E> {

    operator fun get(id: E): Single<T>

    fun delete(item: T): Completable

    fun getAll(page: Int): Single<List<T>>

    fun insert(item: T): Single<T>

    fun update(item: T): Single<T>
}
