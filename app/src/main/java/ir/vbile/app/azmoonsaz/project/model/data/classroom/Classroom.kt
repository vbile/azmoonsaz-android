package ir.vbile.app.azmoonsaz.project.model.data.classroom


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Classroom(
    @SerializedName("exams_count")
    var examsCount: Int?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("name")
    var name: String,
    @SerializedName("user_name")
    var userName: String?
) : Parcelable {
    constructor(name: String) : this(null, null, name, null)
}