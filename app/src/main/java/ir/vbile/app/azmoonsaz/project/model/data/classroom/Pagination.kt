package ir.vbile.app.azmoonsaz.project.model.data.classroom


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Pagination(
    @SerializedName("count")
    var count: Int,
    @SerializedName("current_page")
    var currentPage: Int,
    @SerializedName("per_page")
    var perPage: Int,
    @SerializedName("total")
    var total: Int,
    @SerializedName("total_pages")
    var totalPages: Int
) : Parcelable