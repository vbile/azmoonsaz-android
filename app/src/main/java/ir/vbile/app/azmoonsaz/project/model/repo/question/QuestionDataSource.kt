package ir.vbile.app.azmoonsaz.project.model.repo.question

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.question.QuestionsResponse

interface QuestionDataSource {
    fun getAllQuestions(): Flowable<QuestionsResponse>

    fun getAllQuestions(page: Int): Single<List<QuestionsResponse>>
    fun insertQuestion(
        classroom: Int,
        examId: Int,
        jsonObject: JsonObject
    ): Single<QuestionsResponse>

    fun updateQuestion(
        classroomId: Int,
        examId: Int,
        questionId: Int,
        jsonObject: JsonObject
    ): Single<QuestionsResponse>

    fun deleteQuestion(
        classroomId: Int,
        examId: Int,
        questionId: Int,
        jsonObject: JsonObject
    ): Completable

    fun get(id: Int): Single<QuestionsResponse>


}

