package ir.vbile.app.azmoonsaz.project.core.notification

class NotificationChannels {
  companion object {
    const val PUBLIC = "C_PUBLIC"
    const val DISCOUNT ="C_DISCOUNT"
    const val Q_SERVICE = "C_QSERVICE"
  }
}