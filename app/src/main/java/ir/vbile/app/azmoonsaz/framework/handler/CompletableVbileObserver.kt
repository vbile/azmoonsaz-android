package ir.vbile.app.azmoonsaz.framework.handler

import io.reactivex.CompletableObserver
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.exception.ExceptionMessageFactory

abstract class CompletableVbileObserver(private val activity: BaseActivity) : CompletableObserver {
    override fun onError(e: Throwable) {
        ExceptionMessageFactory.getMessage(e)?.let { activity.showSnackBar(it) }
    }

}