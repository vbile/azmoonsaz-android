package ir.vbile.app.azmoonsaz.framework.exception.http.e422


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class ServerValidationException(
    @SerializedName("errors")
    var errors: Errors,
    @SerializedName("message")
    var message: String
) : Parcelable