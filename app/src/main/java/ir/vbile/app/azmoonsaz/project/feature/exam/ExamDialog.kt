package ir.vbile.app.azmoonsaz.project.feature.exam

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.alirezaafkar.sundatepicker.DatePicker
import com.alirezaafkar.sundatepicker.components.DateItem
import com.alirezaafkar.sundatepicker.components.JDF
import com.alirezaafkar.sundatepicker.interfaces.DateSetListener
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.core.MethodBlock
import ir.vbile.app.azmoonsaz.framework.helper.DateUtil
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.framework.helper.NumberHelper
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class ExamDialog(
    private val mode: Int,
    val onCreate: MethodBlock<Exam> = {},
    val onEdit: MethodBlock<Exam> = {}
) : DialogFragment(),
    View.OnClickListener,
    DateSetListener,
    AdapterView.OnItemSelectedListener {


    // Variables
    private var dialogStartDate = Date()
    private var dialogEndDate = Date()
    private var examStartDate = Date()
    private var examEndDate = Date()


    private lateinit var spinnerTimeAdapter: ArrayAdapter<String>
    private lateinit var spinnerStatusAdapter: ArrayAdapter<String>
    val formatter = SimpleDateFormat("YYYY-MM-dd HH:mm:ss")

    // View
    lateinit var btnClose: FrameLayout
    lateinit var btnCreate: MaterialButton
    lateinit var tvStartDate: TextView
    lateinit var tvEndDate: TextView
    lateinit var dialogTitle: TextView
    lateinit var spinnerStatus: Spinner
    lateinit var spinnerTimes: Spinner
    lateinit var edtExamTitle: TextInputEditText

    // Variables
    lateinit var title: String
    lateinit var startDate: String
    lateinit var endDate: String
    lateinit var status: String
    lateinit var duration: String

    private lateinit var exam: Exam
    private lateinit var classroom: Classroom

    fun initExam(exam: Exam) {
        this.exam = exam
    }

    fun initClassroom(classroom: Classroom) {
        this.classroom = classroom
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(requireContext())
            .inflate(R.layout.dialog_add_exam, null, false)
        setupViews(view)
        dialogStartDate = Date()
        dialogEndDate = Date()
        // Initializing an ArrayAdapter
        spinnerTimeAdapter =
            ArrayAdapter(requireContext(), R.layout.spinner_item_time, L.getTimes())
        spinnerStatusAdapter =
            ArrayAdapter(requireContext(), R.layout.spinner_item_time, L.getExamStatus())
        spinnerTimes.adapter = spinnerTimeAdapter
        spinnerStatus.adapter = spinnerStatusAdapter
        if (mode == MODE_EDIT) {
            val sDate = Date()
            sDate.setDate(JDF(DateUtil.p2g(exam.examStartDate)))
            val eDate = Date()
            eDate.setDate(JDF(DateUtil.p2g(exam.examEndDate)))
            examStartDate = sDate
            examEndDate = eDate
            spinnerTimes.setSelection(0)
            dialogTitle.setText(L.exam_edit)
            updateViews()
            btnCreate.setOnClickListener {
                initVariables()
                validateExam() // ToDo check validate statistics
                initVariablesForExistingExam()
                onEdit(exam)
            }
        } else if (mode == MODE_CREATE) {
            btnCreate.setOnClickListener {
                initVariables()
                validateExam() // ToDo check validate examformatter.format(examStartDate.calendar.time)
                duration = duration.replace(" دقیقه", "")
                exam = Exam(
                    false,
                    classroom,
                    duration,
                    null,
                    NumberHelper.PersianToEnglish(formatter.format(examEndDate.calendar.time)),
                    NumberHelper.PersianToEnglish(formatter.format(examStartDate.calendar.time)),
                    status,
                    title,
                    null
                )
                onCreate(exam)
            }
        } else {
            throw IllegalArgumentException("Please make sure you set dialog mode! CREATE or EDIT")
        }
        val alertDialog = MaterialAlertDialogBuilder(requireContext())
        alertDialog.setView(view)
        return alertDialog.create()
    }

    private fun initVariablesForExistingExam() {
//      status = L.getEnVersionOfStatus(spinnerStatus.selectedItem as String)
        DebugHelper.info(exam)
        duration = spinnerTimes.selectedItem.toString().replace(" دقیقه", "")
        exam.examStatus = L.getEnVersionOfStatus(spinnerStatus.selectedItem as String)
        exam.examTitle = edtExamTitle.text.toString()
        DebugHelper.info(edtExamTitle.text.toString())
        exam.examClass = exam.examClass
        exam.examActive = false
        exam.examDuration = duration
        exam.examStartDate =
            NumberHelper.PersianToEnglish(formatter.format(examStartDate.calendar.time))
        exam.examEndDate =
            NumberHelper.PersianToEnglish(formatter.format(examEndDate.calendar.time))
        DebugHelper.info(exam)
    }

    private fun updateViews() {
        dialogTitle.text = L.exam_edit
        edtExamTitle.setText(exam.examTitle)
        tvStartDate.setText(DateUtil.g2p(exam.examStartDate))
        tvEndDate.setText(DateUtil.g2p(exam.examEndDate))
        val positionSpinnerTimes = spinnerTimeAdapter.getPosition(exam.examDuration + " دقیقه")
        spinnerTimes.setSelection(positionSpinnerTimes)
        val positionSpinnerStatus = spinnerStatusAdapter.getPosition(exam.examStatus)
        spinnerStatus.setSelection(positionSpinnerStatus)
    }

    private fun initVariables() {
        title = edtExamTitle.text.toString()
        startDate = dialogStartDate.calendar.time.toString()
        endDate = dialogEndDate.calendar.time.toString()
        status = L.getEnVersionOfStatus(spinnerStatus.selectedItem as String)
        duration = spinnerTimes.selectedItem as String
    }

    private fun validateExam() {

    }

    private fun setupViews(view: View) {
        btnClose = view.findViewById<FrameLayout>(R.id.btn_dialogCreateExam_dismiss)
        btnCreate = view.findViewById<MaterialButton>(R.id.btn_dialogCreateExam_createExam)
        tvStartDate = view.findViewById<TextView>(R.id.tv_dialogCreateExam_startDate)
        tvEndDate = view.findViewById<TextView>(R.id.tv_dialogCreateExam_endDate)
        spinnerStatus = view.findViewById<Spinner>(R.id.spn_dialog_createExam_status)
        spinnerTimes = view.findViewById<Spinner>(R.id.spn_dialog_createExam_times)
        edtExamTitle = view.findViewById<TextInputEditText>(R.id.edt_dialogCreateExam_name)
        dialogTitle = view.findViewById<TextView>(R.id.tv_dialogExam_title)

        btnClose.setOnClickListener {
            dismiss()
        }
        tvStartDate.setOnClickListener(this)
        tvEndDate.setOnClickListener(this)
        spinnerStatus.onItemSelectedListener = this
        spinnerTimes.onItemSelectedListener = this
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog!!.window!!.getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    override fun onClick(v: View?) {
        val id = if (v!!.getId() === R.id.tv_dialogCreateExam_startDate) 1 else 2
        val minDate = Calendar.getInstance()
        val maxDate = Calendar.getInstance()
        maxDate[Calendar.YEAR] = maxDate[Calendar.YEAR] + 10
        maxDate[Calendar.YEAR] = maxDate[Calendar.YEAR] + 10
        val builder =
            DatePicker.Builder()
                .id(id)
                .minDate(minDate)
                .maxDate(maxDate)
                .date(Calendar.getInstance())
                .setRetainInstance(true)

        if (v?.getId() == R.id.tv_dialogCreateExam_startDate) {
            builder.date(
                dialogStartDate.day,
                dialogStartDate.month,
                dialogStartDate.year
            )
            builder.date(
                examStartDate.day,
                examStartDate.month,
                examStartDate.year
            )
        } else {
            builder.date(dialogEndDate.calendar)
            builder.date(examEndDate.calendar)
        }

        builder.build(this)
            .show(requireActivity().supportFragmentManager, "")
    }

    override fun onDateSet(id: Int, calendar: Calendar?, day: Int, month: Int, year: Int) {
        try {
            if (id == 1) {
                dialogStartDate.setDate(day, month, year)
                examStartDate.setDate(day, month, year)
                tvStartDate.setText(L.exam_start_date + dialogStartDate.date)
                Toast.makeText(
                    requireContext(),
                    "Start =>" + dialogStartDate.date,
                    Toast.LENGTH_LONG
                ).show()
            } else {
                dialogEndDate.setDate(day, month, year)
                examEndDate.setDate(day, month, year)
                tvEndDate.setText(L.exam_end_date + dialogEndDate.date)
                Toast.makeText(
                    requireContext(),
                    "End =>" + dialogEndDate.date,
                    Toast.LENGTH_LONG
                ).show()
            }
        } catch (exception: Exception) {
            // TODO
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        Toast.makeText(
//            requireContext(),
//            "" + parent?.getItemAtPosition(position),
//            Toast.LENGTH_LONG
//        ).show()
    }


    companion object {
        const val MODE_CREATE = 1
        const val MODE_EDIT = 2
    }
}

internal class Date : DateItem() {
    val date: String
        get() {
            val calendar = calendar
            return String.format(
                Locale.US,
                "%d/%d/%d",
                year, month, day,
                calendar[Calendar.YEAR],
                +calendar[Calendar.MONTH] + 1,
                +calendar[Calendar.DAY_OF_MONTH]
            )
        }

    fun date(calendar: Calendar): String {
        return String.format(
            Locale.US,
            "%d/%d/%d",
            year, month, day,
            calendar[Calendar.YEAR],
            +calendar[Calendar.MONTH] + 1,
            +calendar[Calendar.DAY_OF_MONTH]
        )
    }
}