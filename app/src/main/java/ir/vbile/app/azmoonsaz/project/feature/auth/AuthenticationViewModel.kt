package ir.vbile.app.azmoonsaz.project.feature.auth

import android.content.Context
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ir.vbile.app.azmoonsaz.project.model.data.auth.*
import ir.vbile.app.azmoonsaz.project.model.repo.user.UserRepository


class AuthenticationViewModel(private val userRepository: UserRepository) {

    val progressBarMain: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val isInLoginMode = BehaviorSubject.create<Boolean>()

    fun authenticate(
        context: Context,
        mobile: String,
        password: String
    ): Completable {
        val singleToken: Single<Token>
        singleToken = if (isInLoginMode.value == null || isInLoginMode.value!!) {
            userRepository.getToken(
                "password",
                2,
                "grhMlvzuiHZbVfRlVEqt6gFsZJtycHarSHyLfgwk",
                mobile,
                password
            )
        } else {
            val jsonObject = JsonObject()
            jsonObject.addProperty("mobile", mobile)
            jsonObject.addProperty("password", password)
            userRepository.register(jsonObject)
                .flatMap({ successResponse ->
                    userRepository.getToken(
                        "password",
                        2, "grhMlvzuiHZbVfRlVEqt6gFsZJtycHarSHyLfgwk",
                        mobile, password
                    )
                })
        }
        val userInfoManager = UserInfoManager(context)
        return singleToken.doOnSuccess { token: Token ->
            userInfoManager.saveToken(token.accessToken, token.refreshToken)
            TokenContainer.updateToken(token.accessToken)
        }
            .doOnEvent { token: Token?, throwable: Throwable? ->
                progressBarMain.onNext(false)
            }
            .toCompletable()
    }

    fun getUer(): Single<User> {
        TODO("Not yet implemented")
    }


    fun register(
        name: String, family: String, mobile: String, password: String
    ): Single<RegisterResponse> {
        progressBarMain.onNext(true)
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", name)
        jsonObject.addProperty("family", family)
        jsonObject.addProperty("mobile", mobile)
        jsonObject.addProperty("password", password)
        return userRepository.register(jsonObject)
            .doOnEvent { t1, t2 ->
                progressBarMain.onNext(false)
            }
    }

    fun confirm(
        mobile: String,
        password: String,
        otp: String
    ): Single<ServerResponse> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("mobile", mobile)
        jsonObject.addProperty("password", password)
        jsonObject.addProperty("otp", otp)
        progressBarMain.onNext(true)
        return userRepository.confirm(jsonObject)
            .doOnEvent { t1, t2 ->
                progressBarMain.onNext(false)
            }
    }

    fun login(mobile: String, password: String): Single<LoginResponse> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("mobile", mobile)
        jsonObject.addProperty("password", password)
        progressBarMain.onNext(true)
        return userRepository.login(jsonObject).doOnEvent { t1, t2 ->
            progressBarMain.onNext(false)
        }
    }


    fun getIsInLoginMode(): BehaviorSubject<Boolean> {
        return isInLoginMode
    }


}