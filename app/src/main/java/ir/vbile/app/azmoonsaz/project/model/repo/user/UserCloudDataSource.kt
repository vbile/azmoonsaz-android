package ir.vbile.app.azmoonsaz.project.model.repo.user

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.core.service.network.ApiService
import ir.vbile.app.azmoonsaz.project.model.data.auth.*

class UserCloudDataSource(private val apiService: ApiService) : UserDataSource {
    override fun get(id: Int): Single<User> {
        TODO("Not yet implemented")
    }

    override fun delete(item: User): Completable {
        TODO("Not yet implemented")
    }

    override fun getAll(page: Int): Single<List<User>> {
        TODO("Not yet implemented")
    }

    override fun insert(item: User): Single<User> {
        TODO("Not yet implemented")
    }

    override fun update(item: User): Single<User> {
        TODO("Not yet implemented")
    }

    override fun getUer(): Single<User> {
        TODO("Not yet implemented")
    }



    // Authentication Requests
    override fun register(jsonObject: JsonObject): Single<RegisterResponse> {
        return apiService.register(jsonObject)
    }

    override fun confirm(jsonObject: JsonObject): Single<ServerResponse> {
        return apiService.confirm(jsonObject)
    }

    override fun login(jsonObject: JsonObject): Single<LoginResponse> {
        return apiService.login(jsonObject)
    }


    override fun getToken(
        grant_type: String,
        client_id: Int,
        client_secret: String,
        mobile: String,
        password: String
    ): Single<Token> {
        return apiService.getToken(
            grant_type,
            client_id,
            client_secret,
            mobile,
            password
        )
    }
}