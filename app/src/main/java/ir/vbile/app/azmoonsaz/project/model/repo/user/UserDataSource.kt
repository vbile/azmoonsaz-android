package ir.vbile.app.azmoonsaz.project.model.repo.user

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.auth.*
import ir.vbile.app.azmoonsaz.project.model.repo.DataSource

interface UserDataSource : DataSource<User, Int> {
    override fun get(id: Int): Single<User>
    override fun delete(item: User): Completable
    override fun getAll(page: Int): Single<List<User>>
    override fun insert(item: User): Single<User>
    override fun update(item: User): Single<User>
    fun getUer(): Single<User>
    fun register(jsonObject: JsonObject): Single<RegisterResponse>
    fun confirm(jsonObject: JsonObject): Single<ServerResponse>
    fun login(jsonObject: JsonObject): Single<LoginResponse>
    fun getToken(
        grant_type: String,
        client_id: Int,
        client_secret: String,
        mobile: String,
        password: String
    ): Single<Token>
}