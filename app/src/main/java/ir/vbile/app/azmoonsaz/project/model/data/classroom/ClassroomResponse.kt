package ir.vbile.app.azmoonsaz.project.model.data.classroom


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class ClassroomResponse(
    @SerializedName("data")
    var `data`: List<Classroom>,
    @SerializedName("pagination")
    var pagination: Pagination
) : Parcelable