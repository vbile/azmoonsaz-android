package ir.vbile.app.azmoonsaz.framework.handler

import io.reactivex.SingleObserver
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.exception.ExceptionMessageFactory
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper

abstract class SingleVbileObserver<T>(private val activity: BaseActivity) : SingleObserver<T> {

    override fun onError(e: Throwable) {
        e.message?.let { DebugHelper.info(it) }
        ExceptionMessageFactory.getMessage(e)?.let { activity.showSnackBar(it) }
    }
}
