package ir.vbile.app.azmoonsaz.framework.core

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.graphics.Typeface
import android.os.Handler
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.facebook.drawee.backends.pipeline.Fresco
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.config.Configurator
import ir.vbile.app.azmoonsaz.framework.config.lang.EnLang
import ir.vbile.app.azmoonsaz.framework.config.lang.FaLang
import ir.vbile.app.azmoonsaz.framework.config.lang.Lang
import ir.vbile.app.azmoonsaz.framework.helper.ThemeHelper
import ir.vbile.app.azmoonsaz.project.core.service.network.ApiServiceContainer
import ir.vbile.app.azmoonsaz.project.feature.auth.AuthenticationViewModel
import ir.vbile.app.azmoonsaz.project.feature.auth.TokenContainer
import ir.vbile.app.azmoonsaz.project.feature.auth.UserInfoManager
import ir.vbile.app.azmoonsaz.project.feature.classroom.ClassroomViewModel
import ir.vbile.app.azmoonsaz.project.feature.exam.ExamViewModel
import ir.vbile.app.azmoonsaz.project.feature.home.MainViewModel
import ir.vbile.app.azmoonsaz.project.feature.question.QuestionViewModel
import ir.vbile.app.azmoonsaz.project.model.repo.classroom.ClassroomCloudDataSource
import ir.vbile.app.azmoonsaz.project.model.repo.classroom.ClassroomRepository
import ir.vbile.app.azmoonsaz.project.model.repo.exam.ExamCloudDataSource
import ir.vbile.app.azmoonsaz.project.model.repo.exam.ExamRepository
import ir.vbile.app.azmoonsaz.project.model.repo.question.QuestionCloudDataSource
import ir.vbile.app.azmoonsaz.project.model.repo.question.QuestionRepository
import ir.vbile.app.azmoonsaz.project.model.repo.user.UserCloudDataSource
import ir.vbile.app.azmoonsaz.project.model.repo.user.UserRepository
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import java.io.File
import java.util.*


val L = App.language

class App : Application() {

    companion object {
        lateinit var handler: Handler
        lateinit var DIR_SDCARD: File
        lateinit var DIR_APP: String
        lateinit var context: Context
        lateinit var currentActivity: BaseActivity
        lateinit var language: Lang
        lateinit var theme: String
        var locale: String = "fa"
        var layoutDirection: Int = View.LAYOUT_DIRECTION_LTR
        lateinit var defaultFont: Typeface
        var config: Configurator? = null
            set(value) {
                field = value
                value?.config()
                value?.populateNotificationChannels()
            }

        fun initialize() {
            Fresco.initialize(context)
            theme = context.defaultSharedPreferences.getString("theme", "light")!!
            config()
        }

        private fun config() {
            language = FaLang()
            processLanguage()
            handler = Handler()
            TokenContainer.updateToken(UserInfoManager(context).token())
        }

        private fun processLanguage() {
            when (locale) {
                "en" -> {
                    language = EnLang()
                    layoutDirection = View.LAYOUT_DIRECTION_LTR
                    defaultFont = ResourcesCompat.getFont(
                        context,
                        ThemeHelper.getResource("english", "font")
                    )!!
                }
                "fa" -> {
                    language = FaLang()
                    layoutDirection = View.LAYOUT_DIRECTION_RTL
                    defaultFont =
                        ResourcesCompat.getFont(context, ThemeHelper.getResource("farsi", "font"))!!
                }
            }
        }

        fun setLocate(language: String) {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            context.resources.updateConfiguration(
                config,
                context.resources.displayMetrics
            )
        }

    }

    override fun onCreate() {
        super.onCreate()
        context = this.applicationContext
        initialize()
        startKoin()
        DIR_SDCARD = context.getExternalFilesDir(null)!!
        DIR_APP = "$DIR_SDCARD/azmoonsaz/fileformat"
        File(DIR_APP).mkdirs()
        setLocate("fa")
    }


    private fun startKoin() {
        val module = module {
            factory {
                UserRepository(UserCloudDataSource(ApiServiceContainer.getApiService()))
            }

            single {
                AuthenticationViewModel(
                    get(UserRepository::class.java)
                )
            }

            single {
                MainViewModel(
                    get(UserRepository::class.java)
                )
            }


            factory {
                ClassroomRepository(ClassroomCloudDataSource(ApiServiceContainer.getApiService()))
            }
            single {
                ClassroomViewModel(
                    get(ClassroomRepository::class.java)
                )
            }

            factory {
                ExamRepository(ExamCloudDataSource(ApiServiceContainer.getApiService()))
            }
            single {
                ExamViewModel(
                    get(ExamRepository::class.java)
                )
            }

            factory {
                QuestionRepository(QuestionCloudDataSource(ApiServiceContainer.getApiService()))
            }
            single {
                QuestionViewModel(get(QuestionRepository::class.java))
            }


        }
        startKoin {
            androidContext(this@App)
            modules(module)
        }
    }



}