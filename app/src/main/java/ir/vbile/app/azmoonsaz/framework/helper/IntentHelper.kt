package ir.vbile.app.azmoonsaz.framework.helper

import android.app.PendingIntent
import android.content.Intent
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.core.App
import android.provider.Settings
import android.net.Uri

class IntentHelper {
  companion object {
    var lastPendingIntentId = 0
    fun createPendingIntent(clazz: Class<out BaseActivity>): PendingIntent {
      val intent = Intent(App.context, clazz)
      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
      val pendingIntent = PendingIntent.getActivity(App.context, +lastPendingIntentId, intent, PendingIntent.FLAG_UPDATE_CURRENT)
      return pendingIntent
    }

    fun openAppSetiings() {
      val intent = Intent()
      intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
      val uri = Uri.fromParts("package", App.context.packageName, null)
      intent.data = uri
      App.currentActivity.startActivity(intent)
    }
  }
}