package ir.vbile.app.azmoonsaz.framework.provider.image.fresco

import android.net.Uri
import ir.vbile.app.azmoonsaz.framework.component.view.MyImageView
import ir.vbile.app.azmoonsaz.framework.provider.image.ImageLoadingService


class FrescoImageLoadingService : ImageLoadingService {
    override fun loadImage(url: String, imageView: MyImageView) {
        imageView.setImageURI(Uri.parse(url))
    }

    override fun loadImage(imageView: MyImageView, url: String) {
        imageView.setImageURI(Uri.parse(url))
    }

}
