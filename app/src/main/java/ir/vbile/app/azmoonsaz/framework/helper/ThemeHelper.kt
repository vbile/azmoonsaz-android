package ir.vbile.app.azmoonsaz.framework.helper

import ir.vbile.app.azmoonsaz.framework.core.App
import ir.vbile.app.azmoonsaz.framework.core.defaultSharedPreferences

class ThemeHelper {
    companion object {
        fun applyTheme() {
            when (App.theme) {
                "light" -> App.currentActivity.setTheme(
                    getResource(
                        "AppTheme.Light",
                        "style"
                    )
                )
                "dark" -> App.currentActivity.setTheme(
                    getResource(
                        "AppTheme.Dark",
                        "style"
                    )
                )
            }
        }

        fun switchTheme() {
            App.theme = if (App.theme == "light") "dark" else "light"
            App.context.defaultSharedPreferences.edit().putString("theme", App.theme).apply()
            App.currentActivity.recreate()
        }

        fun getResource(name: String, type: String): Int =
            App.context.resources.getIdentifier(name, type, App.context.packageName)

        fun getThemeDrawable(name: String): Int =
            getResource(
                "${name}_${App.theme}",
                "drawable"
            )

    }
}