package ir.vbile.app.azmoonsaz.framework.base


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ir.vbile.app.azmoonsaz.MainActivity
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.core.getActivity
import ir.vbile.app.azmoonsaz.framework.exception.http.UnAuthorizeException
import ir.vbile.app.azmoonsaz.project.feature.auth.OnUserAuthenticate
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


abstract class BaseFragment : Fragment() {
    private var rootView: View? = null
    abstract val getLayoutRes: Int
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (rootView == null) {
            rootView = inflater.inflate(getLayoutRes, container, false)
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    abstract fun setupViews()

    protected fun hideBottomNavigationView() {
        (activity as MainActivity).hideBottomNav()
    }
    protected fun showBottomNavigationView() {
        (activity as MainActivity).showBottomNav()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }
}