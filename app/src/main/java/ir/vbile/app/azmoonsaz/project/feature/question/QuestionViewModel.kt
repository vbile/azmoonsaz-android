package ir.vbile.app.azmoonsaz.project.feature.question

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.question.Question
import ir.vbile.app.azmoonsaz.project.model.data.question.QuestionsResponse
import ir.vbile.app.azmoonsaz.project.model.data.question.one.QuestionUpdateResponse
import ir.vbile.app.azmoonsaz.project.model.repo.question.QuestionRepository

class QuestionViewModel(private var questionRepository: QuestionRepository) {


    fun getAllQuestions(classroom: Int, examId: Int): Flowable<QuestionsResponse> {

        return questionRepository.getAllQuestions(classroom, examId)
    }


    fun insertQuestion(
        classroom: Int, examId: Int, question: Question
    ): Single<Question> {

        val jsonObject = JsonObject()
        jsonObject.addProperty("title", question.question_title)
        jsonObject.addProperty("exam_id", question.question_exam_id)
        jsonObject.addProperty("is_input_mode", question.question_is_input_mode)
        jsonObject.addProperty("correct_answer", question.question_correct_answer)
        jsonObject.addProperty("duration", question.question_duration)
        jsonObject.addProperty("is_limited", question.question_is_limited)

        return questionRepository.insertQuestion(classroom, examId, jsonObject)


    }

    fun updateQuestion(
        classroomId: Int,
        examId: Int,
        questionId: Int, question: Question

    ): Single<QuestionUpdateResponse> {

        val jsonObject = JsonObject()
        jsonObject.addProperty("title", question.question_title)
        jsonObject.addProperty("exam_id", question.question_exam_id)
        jsonObject.addProperty("is_input_mode", question.question_is_input_mode)
        jsonObject.addProperty("correct_answer", question.question_correct_answer)
        jsonObject.addProperty("duration", question.question_duration)
        jsonObject.addProperty("is_limited", question.question_is_limited)


        return questionRepository.updateQuestion(classroomId, examId, questionId, jsonObject)
    }

    fun deleteQuestion(
        classroomId: Int,
        examId: Int,
        questionId: Int

    ): Completable {
        return questionRepository.deleteQuestion(classroomId, examId, questionId)
    }


}