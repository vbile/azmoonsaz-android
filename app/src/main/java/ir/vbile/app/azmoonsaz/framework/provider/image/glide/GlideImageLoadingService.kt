package ir.vbile.app.azmoonsaz.framework.provider.image.glide
import ir.vbile.app.azmoonsaz.framework.component.view.MyImageView
import ir.vbile.app.azmoonsaz.framework.provider.image.ImageLoadingService


class GlideImageLoadingService : ImageLoadingService {
    override fun loadImage(url: String, imageView: MyImageView) {
      //  Glide.with(imageView.context).load(url).into(imageView)
    }

    override fun loadImage(imageView: MyImageView, url: String) {
      //  Glide.with(imageView.context).load(url).into(imageView)
    }

    fun loadImageCircle(imageView: MyImageView, url: String) {
     //   Glide.with(imageView.context).load(url).apply(RequestOptions.circleCropTransform()) .into(imageView)
    }

    fun loadImageCircle(url: String, imageView: MyImageView) {
        //Glide.with(imageView.context).load(url).apply(RequestOptions.circleCropTransform()).into(imageView)

    }
}
