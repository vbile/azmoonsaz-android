package ir.vbile.app.azmoonsaz.project.model.data.classroom.summary


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Meta(
    @SerializedName("total_exams")
    var totalExams: Int,
    @SerializedName("total_participants")
    var totalParticipants: Int
) : Parcelable