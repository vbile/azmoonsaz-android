package ir.vbile.app.azmoonsaz.project.model.data.exam


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom

@SuppressLint("ParcelCreator")
@Parcelize
data class Exam(
    @SerializedName("exam_active")
    var examActive: Boolean,
    @SerializedName("exam_class")
    var examClass: Classroom?,
    @SerializedName("exam_duration")
    var examDuration: String,
    @SerializedName("exam_id")
    var examId: Int?,
    @SerializedName("exam_end_date")
    var examEndDate: String,
    @SerializedName("exam_start_date")
    var examStartDate: String,
    @SerializedName("exam_status")
    var examStatus: String,
    @SerializedName("exam_title")
    var examTitle: String,
    @SerializedName("exam_url")
    var examUrl: String?
) : Parcelable {
    constructor(
        examTitle: String,
        examDuration: String,
        examStartDate: String,
        examEndDate: String,
        examStatus: String,
        examActive: Boolean
    ) : this(
        examActive,
        null,
        examDuration,
        null,
        examEndDate,
        examStartDate,
        examStatus,
        examTitle,
        null
    )
}