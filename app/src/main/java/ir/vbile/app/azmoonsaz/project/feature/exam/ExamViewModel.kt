package ir.vbile.app.azmoonsaz.project.feature.exam

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam
import ir.vbile.app.azmoonsaz.project.model.data.exam.ExamResponse
import ir.vbile.app.azmoonsaz.project.model.data.exam.one.ExamUpdateResponse
import ir.vbile.app.azmoonsaz.project.model.repo.exam.ExamRepository

class ExamViewModel(private val examRepository: ExamRepository) {
    fun getAll(): Flowable<ExamResponse> {
        return examRepository.getAllExams()
    }

    fun update(exam: Exam): Single<ExamUpdateResponse> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("active", exam.examActive)
        jsonObject.addProperty("classroom_id", exam.examClass!!.id)
        jsonObject.addProperty("duration", exam.examDuration)
        jsonObject.addProperty("end_date", exam.examEndDate)
        jsonObject.addProperty("start_date", exam.examStartDate)
        jsonObject.addProperty("status", exam.examStatus)
        jsonObject.addProperty("title", exam.examTitle)
        return examRepository.update(exam.examClass!!.id!!.toInt(), exam.examId!!, jsonObject)
    }

    fun delete(exam: Exam): Completable {
        return examRepository.delete(exam)
    }

    fun getExamsByClassId(classId: Int): Flowable<ExamResponse> {
        return examRepository.getExamsByClassId(classId)
    }

    fun insertExam(classroomId: Int, exam: Exam): Single<Exam> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("title", exam.examTitle)
        jsonObject.addProperty("start_date", exam.examStartDate)
        jsonObject.addProperty("end_date", exam.examEndDate)
        jsonObject.addProperty("duration", exam.examDuration)
        jsonObject.addProperty("active", exam.examActive)
        jsonObject.addProperty("status", exam.examStatus)
        return examRepository.insert(classroomId, jsonObject)
    }

}