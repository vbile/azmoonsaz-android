package ir.vbile.app.azmoonsaz.framework.base

interface OnRvItemClickListener<T> {
    fun onItemClick(item: T, position: Int)
}