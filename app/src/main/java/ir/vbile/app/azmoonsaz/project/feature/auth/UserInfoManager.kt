package ir.vbile.app.azmoonsaz.project.feature.auth

import android.content.Context
import android.content.SharedPreferences

class UserInfoManager(context: Context) {
    private val sharedPreferences: SharedPreferences
    fun saveToken(token: String, refereshToken: String) {
        val editor = sharedPreferences.edit()
        editor.putString("token", token)
        editor.putString("refresh_token", refereshToken)
        editor.apply()
    }

    fun token(): String? {
        return sharedPreferences.getString("token", null)
    }

    fun saveEmail(email: String) {
        val editor = sharedPreferences.edit()
        editor.putString("email", email)
        editor.apply()
    }

    val mobile: String get() = sharedPreferences.getString("email", "")!!

    fun refreshToken() = sharedPreferences.getString("refresh_token", null)!!


    fun clear() = sharedPreferences.edit().clear().apply()


    init {
        sharedPreferences =
            context.getSharedPreferences("user_info", Context.MODE_PRIVATE)
    }
}