package ir.vbile.app.azmoonsaz.framework.provider.image
import ir.vbile.app.azmoonsaz.framework.component.view.MyImageView

interface ImageLoadingService {
    fun loadImage(url: String, imageView: MyImageView)
    fun loadImage(imageView: MyImageView, url: String)
}