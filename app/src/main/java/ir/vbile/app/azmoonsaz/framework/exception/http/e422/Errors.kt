package ir.vbile.app.azmoonsaz.framework.exception.http.e422


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Errors(
    @SerializedName("mobile")
    var mobile: List<String>
) : Parcelable