package ir.vbile.app.azmoonsaz.framework.handler

import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import ir.vbile.app.azmoonsaz.framework.core.App
import ir.vbile.app.azmoonsaz.framework.core.FunctionBlock
import ir.vbile.app.azmoonsaz.framework.core.MethodBlock
import ir.vbile.app.azmoonsaz.framework.helper.HelperDialog
import ir.vbile.app.azmoonsaz.framework.helper.IntentHelper

class PermissionHandler(val permissions: List<String>, props: MethodBlock<PermissionHandler>) {
    companion object {
        var lastRequestCode: Int = 0
    }

    private var requestCode: Int = ++lastRequestCode
    private var activity = App.currentActivity
    var onGrant: FunctionBlock? = null
    var whyPermissionRequired: String = "Please grant all permissions..."
    var whyPermissionRequiredTitle: String = "Permission Required"

    init {
        this.props()
        activity.addPermissionHandler(this)
        request()
    }

    private fun canShowReason(): Boolean {
        permissions.forEach { permission ->
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                return true
            }
        }
        return false
    }

    private fun request() {
        if (isGranted()) {
            return
        }
        if (canShowReason()) {
            HelperDialog.alertOkCancel(
                title = whyPermissionRequiredTitle,
                message = whyPermissionRequired,
                onOk = {
                    requestPermission()
                },
                onCancel = {
                    activity.finish()
                }
            )
        } else {
            requestPermission()
        }
    }


    private fun requestPermission() {
        ActivityCompat.requestPermissions(activity, permissions.toTypedArray(), requestCode)
    }

    private fun isGranted(): Boolean {

        permissions.forEach { permission ->
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(
                    App.context,
                    permission
                )
            ) {
                return false
            }
        }
        return true
    }

    fun processOnPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean {
        if (requestCode != this.requestCode) {
            return false
        }

        // It's mine
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (onGrant != null) {
                onGrant!!.invoke()
            }
            return true
        }
        if (canShowReason()) {
            request()
            // denied
        } else {
            // always denied

            HelperDialog.alertOkCancel(
                title = whyPermissionRequiredTitle,
                message = whyPermissionRequired,
                onOk = {
                    IntentHelper.openAppSetiings()
                    App.currentActivity.finish()
                },
                onCancel = {
                    activity.finish()
                }
            )
        }

        return true
    }
}

