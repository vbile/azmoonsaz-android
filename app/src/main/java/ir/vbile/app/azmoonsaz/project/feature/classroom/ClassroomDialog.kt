package ir.vbile.app.azmoonsaz.project.feature.classroom

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.core.FunctionBlock
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.core.MethodBlock
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom


class ClassroomDialog(
    private val mode: Int,
    val onClicked: MethodBlock<String> = {}
) : DialogFragment() {
    private lateinit var classroom: Classroom

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view =
            LayoutInflater.from(requireContext()).inflate(R.layout.dialog_classroom, null, false)
        val title = view.findViewById<TextView>(R.id.tv_dialogClassroom_title)
        val btn = view.findViewById<MaterialButton>(R.id.btn_dialogClassroom)
        val name = view.findViewById<TextInputEditText>(R.id.edt_dialogClass_classroomName)
        val close = view.findViewById<FrameLayout>(R.id.btn_dialogClassroom_dismiss)
        if (mode == MODE_EDIT) {
            name.setText(classroom.name)
            title.text = L.classroom_edit
            btn.text = L.classroom_edit
        } else {
            title.setText(L.classroom_create)
            btn.text = L.classroom_create
        }
        close.setOnClickListener {
            dismiss()
        }
        btn.setOnClickListener {
            onClicked(name.text.toString().trim())
        }
        val builder = MaterialAlertDialogBuilder(requireContext())
        builder.setView(view)
        return builder.create()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog!!.window!!.getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    fun initClassroom(classroom: Classroom) {
        this.classroom = classroom
    }

    companion object {
        const val MODE_CREATE = 1
        const val MODE_EDIT = 2

    }
}