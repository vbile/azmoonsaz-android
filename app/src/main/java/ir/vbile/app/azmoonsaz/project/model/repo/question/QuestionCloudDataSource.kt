package ir.vbile.app.azmoonsaz.project.model.repo.question

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.core.service.network.ApiService
import ir.vbile.app.azmoonsaz.project.model.data.question.Question
import ir.vbile.app.azmoonsaz.project.model.data.question.QuestionsResponse
import ir.vbile.app.azmoonsaz.project.model.data.question.one.QuestionUpdateResponse

class QuestionCloudDataSource(private val apiService: ApiService) {
    fun getAllQuestions(classroom: Int, examId: Int): Flowable<QuestionsResponse> {

        return apiService.getAllQuestions(classroom, examId)
    }

    fun getAllQuestions(page: Int): Single<List<QuestionsResponse>> {
        TODO("Not yet implemented")
    }

    fun insertQuestion(
        classroom: Int,
        examId: Int,
        jsonObject: JsonObject
    ): Single<Question> {
        return apiService.insertQuestion(classroom, examId, jsonObject)
            .flatMap {
                return@flatMap Single.create<Question>({ emitter ->
                    try {
                        if (!emitter.isDisposed) {
                            emitter.onSuccess(it.question)
                        }
                    } catch (exception: Exception) {
                        if (!emitter.isDisposed) {
                            emitter.onError(exception)
                        }
                    }
                })
            }
    }

    fun updateQuestion(
        classroomId: Int,
        examId: Int,
        questionId: Int,
        jsonObject: JsonObject
    ): Single<QuestionUpdateResponse> {
        return apiService.updateQuestion(classroomId, examId, questionId, jsonObject)
    }

    fun deleteQuestion(
        classroomId: Int,
        examId: Int,
        questionId: Int

    ): Completable {
        return apiService.deleteQuestion(classroomId, examId, questionId)
    }

    fun get(id: Int): Single<QuestionsResponse> {
        TODO("Not yet implemented")
    }

}