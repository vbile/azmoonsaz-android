package ir.vbile.app.azmoonsaz.project.feature.exam

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.core.FunctionBlock
import kotlinx.android.synthetic.main.dialog_delete.*

class MaterialDialog constructor(
    val title: String? = null,
    val message: String,
    val onYes: FunctionBlock = {},
    val onNo: FunctionBlock = {}
) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = MaterialAlertDialogBuilder(requireContext())
        val view =
            LayoutInflater.from(requireContext()).inflate(R.layout.dialog_delete, null, false)
        val btnYes = view.findViewById<MaterialButton>(R.id.btn_dialogDelete_yes)
        val btnNo = view.findViewById<MaterialButton>(R.id.btn_dialogDelete_no)
        val tvTitle = view.findViewById<TextView>(R.id.tv_dialogDelete_title)
        val tvMessage = view.findViewById<TextView>(R.id.tv_dialogDelete_message)
        btnYes.setOnClickListener { onYes() }
        btnNo.setOnClickListener { onNo() }
        tvTitle.text = title
        tvMessage.text = message
        dialog.setView(view)
        return dialog.create()
    }
}