package ir.vbile.app.azmoonsaz.framework.base

import io.reactivex.disposables.CompositeDisposable
import org.reactivestreams.Subscription

abstract class ObserverFragment : BaseFragment() {
    protected var compositeDisposable = CompositeDisposable()
    protected var isSubscribeCalled = false
    protected var subscription: Subscription? = null

    override fun onStart() {
        super.onStart()
        if (!isSubscribeCalled) {
            subscribe()
            isSubscribeCalled = true
        }
    }

    override fun onStop() {
        super.onStop()
        unSubscribe()
    }

    abstract fun subscribe()

    fun unSubscribe() {
        compositeDisposable.clear()
    }

    override fun onDetach() {
        super.onDetach()
        subscription?.cancel()
    }
}