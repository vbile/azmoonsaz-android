package ir.vbile.app.azmoonsaz.project.core.service.network

import com.google.gson.GsonBuilder
import ir.vbile.app.azmoonsaz.project.feature.auth.TokenContainer.token
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiServiceContainer {
    private var apiService: ApiService? = null
    fun getApiService(): ApiService {
        if (apiService == null) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain ->
                val original = chain.request()
                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                    .header("Authorization", "Bearer " + token) // <-- this is the important line
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                val request = requestBuilder.build()
                chain.proceed(request)
            }

            httpClient.connectTimeout(30, TimeUnit.SECONDS)
            httpClient.readTimeout(30, TimeUnit.SECONDS)
            httpClient.addNetworkInterceptor(logging)
            val okHttpClient = httpClient.build()
            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .baseUrl(EndPoints.API_BASE_URL)
                .addConverterFactory(
                    GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
            apiService = retrofit.create(ApiService::class.java)
        }
        return apiService!!
    }
}