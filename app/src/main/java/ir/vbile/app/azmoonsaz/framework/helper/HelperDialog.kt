package ir.vbile.app.azmoonsaz.framework.helper

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AlertDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.core.App
import ir.vbile.app.azmoonsaz.framework.core.FunctionBlock
import ir.vbile.app.azmoonsaz.framework.core.L

class HelperDialog {
    companion object {

        // AlertDialog
        private fun alertCommonBuilder(
            message: String,
            title: String? = null,
            justReturn: Boolean = false
        ): AlertDialog.Builder {
            return AlertDialog.Builder(App.currentActivity)
                .setMessage(message)
                .setTitle(title).apply {
                    if (!justReturn) {
                        create()
                        show()
                    }
                }
        }

        fun alertOk(
            message: String,
            title: String? = null,
            onOk: FunctionBlock = {},
            justReturn: Boolean = false
        ): AlertDialog.Builder {
            return alertCommonBuilder(
                message,
                title
            )
                .setPositiveButton(L.button_ok, { dialog, which ->
                    onOk()
                }).apply {
                    if (!justReturn) {
                        create()
                        show()
                    }
                }
        }

        fun alertOkCancel(
            message: String,
            title: String? = null,
            onOk: FunctionBlock = {},
            onCancel: FunctionBlock = {},
            justReturn: Boolean = false
        ): AlertDialog.Builder {
            return alertOk(
                message,
                title,
                onOk,
                true
            )
                .setNeutralButton(L.button_cancel, { dialog, which ->
                    onCancel()
                }).apply {
                    if (!justReturn) {
                        create()
                        show()
                    }
                }
        }

        fun alertYesNo(
            message: String,
            title: String? = null,
            onYes: FunctionBlock = {},
            onNo: FunctionBlock = {},
            justReturn: Boolean = false
        ): AlertDialog.Builder {
            return alertCommonBuilder(
                message,
                title
            )
                .setPositiveButton(L.button_yes, { dialog, which ->
                    onYes()
                })
                .setNegativeButton(L.button_no, { dialog, which -> onNo() }).apply {
                    if (!justReturn) {
                        create()
                        show()
                    }
                }

        }

        fun alertYesNoCancel(
            message: String,
            title: String? = null,
            onYes: FunctionBlock = {},
            onNo: FunctionBlock = {},
            onCancel: FunctionBlock = {},
            justReturn: Boolean = false
        ): AlertDialog.Builder {
            return alertYesNo(
                message,
                title,
                onYes,
                onNo,
                true
            )
                .setNeutralButton(L.button_cancel, { dialog, which ->
                    onCancel()
                }).apply {
                    if (!justReturn) {
                        create()
                        show()
                    }
                }
        }


        // MaterialAlertDialogBuilder
        fun alertYesNoNew(
            message: String,
            title: String? = null,
            onYes: FunctionBlock = {},
            onNo: FunctionBlock = {},
            justReturn: Boolean = false
        ) {
            MaterialAlertDialogBuilder(App.currentActivity, R.style.AlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(L.button_no) { dialog, which ->
                    onNo()
                }
                .setPositiveButton(L.button_ok) { dialog, which ->
                    onYes()
                }.apply {
                    if (!justReturn) {
                        create()
                        show()
                    }
                }
        }
    }
}