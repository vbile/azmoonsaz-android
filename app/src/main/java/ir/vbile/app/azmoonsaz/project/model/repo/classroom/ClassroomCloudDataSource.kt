package ir.vbile.app.azmoonsaz.project.model.repo.classroom

import androidx.databinding.Observable
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.SingleSource
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.project.core.service.network.ApiService
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.classroom.ClassroomResponse
import ir.vbile.app.azmoonsaz.project.model.data.classroom.summary.ClassroomSummaryResponse
import ir.vbile.app.azmoonsaz.project.model.data.classroom.update.ClassroomUpdateResponse
import java.lang.Exception

class ClassroomCloudDataSource(private val apiService: ApiService) {
    fun get(id: Int): Single<ClassroomResponse> {
        TODO("Not yet implemented")
    }

    fun delete(item: ClassroomResponse): Completable {
        TODO("Not yet implemented")
    }

    fun getAll(page: Int): Single<List<ClassroomResponse>> {
        TODO("Not yet implemented")
    }

    fun insert(item: ClassroomResponse): Single<ClassroomResponse> {
        TODO("Not yet implemented")
    }

    fun update(classroomId: Int, jsonObject: JsonObject): Single<Classroom> {
        return apiService.updateClassroom(classroomId, jsonObject)
            .flatMap { classroomResponse ->
                return@flatMap Single.create<Classroom> {
                    try {
                        if (!it.isDisposed) {
                            it.onSuccess(classroomResponse.data)
                        }
                    } catch (exception: Exception) {
                        if (!it.isDisposed) {
                            it.onError(exception)
                        }
                        DebugHelper.info(exception.message!!)
                    }
                }
            }
    }


    // Classroom Requests
    fun createClassroom(jsonObject: JsonObject): Single<Classroom> {
        return apiService.createClassroom(jsonObject)
    }

    fun getClassrooms(page: Int): Single<ClassroomResponse> {
        return apiService.getClassrooms(page)
    }

    fun getClassroomSummary(): Single<ClassroomSummaryResponse> {
        return apiService.getClassroomSummary()
    }
}