package ir.vbile.app.azmoonsaz.framework.handler

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import ir.vbile.app.azmoonsaz.framework.core.App
import ir.vbile.app.azmoonsaz.framework.core.MethodBlock

class NotificationHandler {
    class Builder(@DrawableRes var icon: Int, props: MethodBlock<Builder> = {}) {
        companion object {
            private var lastNotificationId = 0

        }

        private var notificationId: Int = ++lastNotificationId
        private var actions = mutableListOf<Action>()
        var channelId: String = "default"
        var title: String = "Notification"
        var text: String = "Fill your notification text"
        var priority: Int = NotificationCompat.PRIORITY_DEFAULT
        var pendingIntent: PendingIntent? = null

        init {
            this.props()
            show()
        }


        private data class Action(
            @DrawableRes var icon: Int,
            var caption: String,
            var pendingIntent: PendingIntent?
        )

        fun addAction(@DrawableRes icon: Int, caption: String, pendingIntent: PendingIntent?) {
            actions.add(Action(icon, caption, pendingIntent))
        }

        private fun show() {
            NotificationManagerCompat.from(App.context).notify(
                notificationId, NotificationCompat.Builder(App.context, channelId)
                    .setSmallIcon(icon)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setPriority(priority)
                    .apply {
                        if (pendingIntent != null) {
                            setContentIntent(pendingIntent)
                        }
                        for (action in actions) {
                            addAction(action.icon, action.caption, action.pendingIntent)
                        }
                    }
                    .build()
            )
        }
    }

    class Channel(var id: String, var props: MethodBlock<Channel> = {}) {
        var name: String = "Default Channel"
        var description: String = "Default Channel Description"
        var importance: Int? = null
        var enabledLights: Boolean = false
        var vibration: Boolean = false

        init {
            this.props()
            create()
        }

        private fun create() {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (importance == null) {
                    importance = NotificationManager.IMPORTANCE_DEFAULT
                }
                val channel = NotificationChannel(id, name, importance!!).apply {
                    description = this@Channel.description
                    enableLights(enabledLights)
                    enableVibration(vibration)
                }
                val manager =
                    App.context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                manager.createNotificationChannel(channel)
            }
        }
    }
}