package ir.vbile.app.azmoonsaz.framework.exception.http.e401


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class AuthorizationException(
    @SerializedName("error")
    var error: String,
    @SerializedName("error_description")
    var errorDescription: String,
    @SerializedName("message")
    var message: String
) : Parcelable