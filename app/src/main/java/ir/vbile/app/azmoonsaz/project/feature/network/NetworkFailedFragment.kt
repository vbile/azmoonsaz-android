package ir.vbile.app.azmoonsaz.project.feature.network

import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import kotlinx.android.synthetic.main.fragment_network_failed.*

class NetworkFailedFragment : ObserverFragment() {
    override fun subscribe() {

    }

    override val getLayoutRes: Int get() = R.layout.fragment_network_failed

    override fun setupViews() {
        hideBottomNavigationView()
        btn_fragmentNetworkFailed_try.setOnClickListener {
            (activity as BaseActivity).recreate()
        }
    }
}