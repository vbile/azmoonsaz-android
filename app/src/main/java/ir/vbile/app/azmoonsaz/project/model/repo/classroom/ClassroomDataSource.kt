package ir.vbile.app.azmoonsaz.project.model.repo.classroom

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.classroom.ClassroomResponse
import ir.vbile.app.azmoonsaz.project.model.repo.DataSource
import ir.vbile.app.azmoonsaz.project.model.data.classroom.summary.ClassroomSummaryResponse

interface ClassroomDataSource : DataSource<ClassroomResponse, Int> {
    override fun get(id: Int): Single<ClassroomResponse>
    override fun delete(item: ClassroomResponse): Completable
    override fun getAll(page: Int): Single<List<ClassroomResponse>>
    override fun insert(item: ClassroomResponse): Single<ClassroomResponse>
    override fun update(item: ClassroomResponse): Single<ClassroomResponse>
    fun createClassroom(jsonObject: JsonObject): Single<Classroom>
    fun getClassrooms(page: Int): Single<ClassroomResponse>
    fun getClassroomSummary(): Single<ClassroomSummaryResponse>
}