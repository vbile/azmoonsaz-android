package ir.vbile.app.azmoonsaz.project.core

import ir.vbile.app.azmoonsaz.framework.config.Configurator
import ir.vbile.app.azmoonsaz.framework.handler.NotificationHandler
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.framework.helper.LOG_LEVEL
import ir.vbile.app.azmoonsaz.project.core.notification.NotificationChannels
import ir.vbile.app.azmoonsaz.project.feature.auth.TokenContainer
import ir.vbile.app.azmoonsaz.project.feature.auth.UserInfoManager

class Config : Configurator {
    override fun config() {
        DebugHelper.logTag = "AzmoonSaz"
        DebugHelper.level = LOG_LEVEL.VERBOSE
    }


    override fun populateNotificationChannels() {
        NotificationHandler.Channel(NotificationChannels.PUBLIC) {
            description = "Global news profile everything..."
            name = "Discount and offers"
        }
        NotificationHandler.Channel(NotificationChannels.DISCOUNT) {
            description = "Discount and Offers"
            name = "Update profile any offers  and discount..."
        }
        NotificationHandler.Channel(NotificationChannels.Q_SERVICE) {
            description = "Question/Answer Service"
            name = "When you recive a notify in question service, we will notify here."
        }
    }

}