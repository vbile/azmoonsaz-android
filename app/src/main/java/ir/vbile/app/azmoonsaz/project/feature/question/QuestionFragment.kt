package ir.vbile.app.azmoonsaz.project.feature.question

import android.util.Log
import androidx.navigation.fragment.navArgs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.core.getActivity
import ir.vbile.app.azmoonsaz.framework.exception.ExceptionMessageFactory
import ir.vbile.app.azmoonsaz.framework.handler.CompletableVbileObserver
import ir.vbile.app.azmoonsaz.framework.handler.SingleVbileObserver
import ir.vbile.app.azmoonsaz.project.feature.classroom.ClassroomFragment
import ir.vbile.app.azmoonsaz.project.feature.classroom.OnClassroomUpdate
import ir.vbile.app.azmoonsaz.project.model.data.question.Question
import ir.vbile.app.azmoonsaz.project.model.data.question.one.QuestionUpdateResponse
import kotlinx.android.synthetic.main.fragment_question.*
import org.greenrobot.eventbus.EventBus
import org.koin.android.ext.android.inject

class QuestionFragment : ObserverFragment() {
    override val getLayoutRes: Int = R.layout.fragment_question
    private val args by navArgs<QuestionFragmentArgs>()
    private val questionViewModel by inject<QuestionViewModel>()
    override fun setupViews() {
        //  tv_fragmentQuestion_examTitle.text = args.exam.examTitle
    }

    override fun subscribe() {
        hideBottomNavigationView()

        btn_fragmentQuestion_addDescriptiveQuestions.setOnClickListener {

            var question = Question(
                "1", "20:00", args.exam.examId!!,
                1, 1, "سوال ششم"
            )


            // insertQuestion(args.exam.examClass!!.id!!,args.exam.examId!!,question)


        }


        getAllQuestions(args.exam.examClass!!.id!!, args.exam.examId!!)

    }


    fun getAllQuestions(classroom: Int, examId: Int) {
        questionViewModel.getAllQuestions(classroom, examId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                if (it?.question != null && it.question.size > 0) {

                    Log.e("TAG", it.question.size.toString())

                } else {
                    // ToDo show empty state
                    getActivity.showToast(L.exam_not_exist)
                }
            }
            .doOnError {
                ExceptionMessageFactory.getMessage(it)
            }
            .doOnSubscribe {
                subscription = it
            }
            .subscribe()


    }


    fun insertQuestion(classroom: Int, examId: Int, question: Question) {

        questionViewModel.insertQuestion(classroom, examId, question)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<Question>(getActivity) {
                override fun onSuccess(question: Question) {
                    EventBus.getDefault().post(OnClassroomUpdate())
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }


            })


    }

    fun updateQuestion(classroomId: Int, examId: Int, questionId: Int, question: Question) {

        questionViewModel.updateQuestion(classroomId, examId, questionId, question)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<QuestionUpdateResponse>(getActivity) {
                override fun onSuccess(questionUpdateResponse: QuestionUpdateResponse) {

                    EventBus.getDefault().post(OnClassroomUpdate())
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })


    }

    fun deleteQuestion(classroomId: Int, examId: Int, questionId: Int) {

        questionViewModel.deleteQuestion(classroomId, examId, questionId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableVbileObserver(getActivity) {
                override fun onComplete() {

                    getActivity.showToast(L.exam_deleted_successfully)

                    EventBus.getDefault().post(OnClassroomUpdate())


                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })

    }


    override fun onDestroy() {
        super.onDestroy()
        showBottomNavigationView()
    }


}