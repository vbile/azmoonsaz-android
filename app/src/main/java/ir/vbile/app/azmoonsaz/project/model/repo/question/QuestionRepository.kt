package ir.vbile.app.azmoonsaz.project.model.repo.question

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.question.Question
import ir.vbile.app.azmoonsaz.project.model.data.question.QuestionsResponse
import ir.vbile.app.azmoonsaz.project.model.data.question.one.QuestionUpdateResponse

class QuestionRepository(private val questionCloudDataSource: QuestionCloudDataSource) {

    fun getAllQuestions(classroom: Int, examId: Int): Flowable<QuestionsResponse> {

        return questionCloudDataSource.getAllQuestions(classroom, examId)
    }


    fun insertQuestion(
        classroom: Int, examId: Int, jsonObject: JsonObject
    ): Single<Question> {
        return questionCloudDataSource.insertQuestion(classroom, examId, jsonObject)
    }

    fun updateQuestion(
        classroomId: Int,
        examId: Int,
        questionId: Int,
        jsonObject: JsonObject
    ): Single<QuestionUpdateResponse> {
        return questionCloudDataSource.updateQuestion(classroomId, examId, questionId, jsonObject)
    }

    fun deleteQuestion(
        classroomId: Int,
        examId: Int,
        questionId: Int

    ): Completable {
        return questionCloudDataSource.deleteQuestion(classroomId, examId, questionId)
    }


}