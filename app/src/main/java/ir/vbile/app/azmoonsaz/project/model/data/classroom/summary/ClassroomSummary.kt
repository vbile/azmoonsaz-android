package ir.vbile.app.azmoonsaz.project.model.data.classroom.summary


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class ClassroomSummary(
    @SerializedName("classroom")
    var classroom: Classroom,
    @SerializedName("classroom_exam_count")
    var classroomExamCount: Int,
    @SerializedName("classroom_participants")
    var classroomParticipants: Int
) : Parcelable