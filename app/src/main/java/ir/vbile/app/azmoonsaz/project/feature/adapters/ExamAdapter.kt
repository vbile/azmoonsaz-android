package ir.vbile.app.azmoonsaz.project.feature.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.button.MaterialButton
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseAdapter
import ir.vbile.app.azmoonsaz.framework.base.BaseViewHolder
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.helper.DateUtil
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam
import java.util.*

class ExamAdapter(private var exams: List<Exam> = arrayListOf()) :
    BaseAdapter<Exam, ExamAdapter.ExamViewHolder>(exams as ArrayList<Exam>) {

    private var examEventListener: ExamItemEvenListener? = null

    inner class ExamViewHolder(item: View) : BaseViewHolder<Exam>(item) {
        private val tvTitle: TextView
        private val tvStartDate: TextView
        private val tvEndDate: TextView
        private val tvStudents: TextView
        private val tvStatus: TextView
        private val tvDuration: TextView
        private val btnDelete: MaterialButton
        private val btnEdit: MaterialButton
        private val btnPublish: MaterialButton

        init {
            tvTitle = itemView.findViewById(R.id.tv_itemExam_name)
            tvStartDate = itemView.findViewById(R.id.tv_itemExam_startDate)
            tvStatus = itemView.findViewById(R.id.tv_itemExam_status)
            tvDuration = itemView.findViewById(R.id.tv_itemExam_duration)
            tvEndDate = itemView.findViewById(R.id.tv_itemExam_endDate)
            tvStudents = itemView.findViewById(R.id.tv_itemExam_students)
            btnDelete = itemView.findViewById(R.id.btn_itemExam_delete)
            btnEdit = itemView.findViewById(R.id.btn_itemExam_edit)
            btnPublish = itemView.findViewById(R.id.btn_itemExam_publish)
        }

        override fun bind(item: Exam) {
            tvStartDate.text = L.getExamStartDate(DateUtil.g2p(item.examStartDate))
            tvEndDate.text = L.getExamEndDate(DateUtil.g2p(item.examEndDate))
            tvStatus.text = L.getExamsStudentsCount(item.examClass!!.examsCount)
            tvTitle.text = item.examTitle
            tvStatus.text = item.examStatus
            tvDuration.text = L.getExamDuration(item.examDuration)
            btnDelete.setOnClickListener {
                examEventListener?.onDelete(exams[adapterPosition], adapterPosition)
            }
            btnEdit.setOnClickListener {
                examEventListener?.onEdit(exams[adapterPosition], adapterPosition)

            }
            btnPublish.setOnClickListener {
                examEventListener?.onPublish(exams[adapterPosition], adapterPosition)
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_exam, parent, false)
        return ExamViewHolder(view)
    }

    interface ExamItemEvenListener {
        fun onEdit(exam: Exam, position: Int)
        fun onDelete(exam: Exam, position: Int)
        fun onPublish(exam: Exam, position: Int)
    }


    fun setOnExamItemEventListener(examEventListener: ExamItemEvenListener) {
        this.examEventListener = examEventListener
    }

}