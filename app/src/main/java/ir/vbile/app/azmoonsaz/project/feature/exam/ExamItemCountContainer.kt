package ir.vbile.app.azmoonsaz.project.feature.exam

object ExamItemCountContainer {
    private var count = 0
    fun updateCount(count: Int) {
        ExamItemCountContainer.count = count
    }
}