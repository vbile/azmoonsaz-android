package ir.vbile.app.azmoonsaz.project.core.service.network

object EndPoints {
    const val MAIN_URL = "https://project.vbile.ir/azmoonsaz"
    const val API_BASE_URL = MAIN_URL + "/api/v1/"

    // Authentication URLs
    const val REGISTER_URL = API_BASE_URL + "auth/register"
    const val CONFIRM_URL = API_BASE_URL + "auth/confirm"
    const val LOGIN_URL = API_BASE_URL + "auth/login"
    const val TOKEN_URL = MAIN_URL + "/oauth/token"

    // Classroom URLs
    const val GET_ALL_CLASSROOM = API_BASE_URL + "teachers/classrooms/"
    const val GET_CLASSROOM_SUMMARY = API_BASE_URL + "teachers/classrooms/summary"
    const val CREATE_CLASSROOM = API_BASE_URL + "teachers/classrooms/store"
    const val UPDATE_CLASSROOM = API_BASE_URL + "teachers/classrooms/{classroom}/update"


    // Exam URLs
    const val GET_ALL_EXAMS = API_BASE_URL + "teachers/exams"
    const val INSERT_EXAM = API_BASE_URL + "teachers/classrooms/{classroom}/exams/add"
    const val GET_EXAMS_BY_CLASSROOM_ID = API_BASE_URL + "teachers/classrooms/{classroom}/exams/"
    const val UPDATE_EXAM =
        API_BASE_URL + "teachers/classrooms/{classroom}/exams/{statistics}/update"
    const val DELETE_EXAM =
        API_BASE_URL + "teachers/classrooms/{classroom}/exams/{statistics}/delete"
    const val RESTORE_EXAM =
        API_BASE_URL + "teachers/classrooms/{classroom}/exams/{statistics}/restore"


    //Questions URLS

    const val GET_ALL_QUESTIONS =
        API_BASE_URL + "teachers/classrooms/{classroom}/exams/{exams}/questions"
    const val INSERT_QUESTION =
        API_BASE_URL + "teachers/classrooms/{classroom}/exams/{exams}/questions/add"
    const val SHOW_QUESTIONS =
        API_BASE_URL + "teachers/classrooms/{classroom}/exams/{exams}/questions/{questions}/show"
    const val UPDATE_QUESTIONS =
        API_BASE_URL + "teachers/classrooms/{classroom}/exams/{exams}/questions/{questions}/update"
    const val DELETE_QUESTIONS =
        API_BASE_URL + "/api/v1/teachers/classrooms/{classroom}/exams/{exams}/questions/{questions}/delete"
    const val RESTORE_QUESTIONS =
        API_BASE_URL + "/api/v1/teachers/classrooms/{classroom}/exams/{exams}/questions/{questions}/restore"


}