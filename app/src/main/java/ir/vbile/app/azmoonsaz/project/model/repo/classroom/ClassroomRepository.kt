package ir.vbile.app.azmoonsaz.project.model.repo.classroom

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom
import ir.vbile.app.azmoonsaz.project.model.data.classroom.ClassroomResponse
import ir.vbile.app.azmoonsaz.project.model.repo.DataSource
import ir.vbile.app.azmoonsaz.project.model.data.classroom.summary.ClassroomSummaryResponse
import ir.vbile.app.azmoonsaz.project.model.data.classroom.update.ClassroomUpdateResponse

class ClassroomRepository(private val classroomCloudDataSource: ClassroomCloudDataSource) {
     fun get(id: Int): Single<ClassroomResponse> {
        TODO("Not yet implemented")
    }

     fun delete(item: ClassroomResponse): Completable {
        TODO("Not yet implemented")
    }

     fun getAll(page: Int): Single<List<ClassroomResponse>> {
        TODO("Not yet implemented")
    }

     fun insert(item: ClassroomResponse): Single<ClassroomResponse> {
        TODO("Not yet implemented")
    }

     fun update(classroomId: Int , jsonObject: JsonObject): Single<Classroom> {
        return  classroomCloudDataSource.update(classroomId, jsonObject)
    }

    fun createClassroom(jsonObject: JsonObject): Single<Classroom>  {
        return classroomCloudDataSource.createClassroom(jsonObject)
    }

    fun getClassrooms(page: Int): Single<ClassroomResponse> {
        return classroomCloudDataSource.getClassrooms(page)
    }

    fun getClassroomSummary(): Single<ClassroomSummaryResponse> {
        return  classroomCloudDataSource.getClassroomSummary()
    }

}