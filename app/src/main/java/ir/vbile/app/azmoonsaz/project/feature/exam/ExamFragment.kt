package ir.vbile.app.azmoonsaz.project.feature.exam


import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.reactivex.disposables.Disposable
import ir.vbile.app.azmoonsaz.framework.handler.SingleVbileObserver


import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import ir.vbile.app.azmoonsaz.framework.base.OnRvItemClickListener
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.core.getActivity
import ir.vbile.app.azmoonsaz.framework.exception.ExceptionMessageFactory
import ir.vbile.app.azmoonsaz.framework.handler.CompletableVbileObserver
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.project.feature.adapters.ExamAdapter
import ir.vbile.app.azmoonsaz.project.feature.auth.OnUserAuthenticate
import ir.vbile.app.azmoonsaz.project.feature.classroom.ClassroomFragment
import ir.vbile.app.azmoonsaz.project.feature.classroom.OnClassroomUpdate
import ir.vbile.app.azmoonsaz.project.feature.exam.ExamDialog.Companion.MODE_CREATE
import ir.vbile.app.azmoonsaz.project.feature.exam.ExamDialog.Companion.MODE_EDIT
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam
import ir.vbile.app.azmoonsaz.project.model.data.exam.one.ExamUpdateResponse
import kotlinx.android.synthetic.main.fragment_exam.*
import org.greenrobot.eventbus.EventBus
import org.koin.android.ext.android.inject


class ExamFragment : ObserverFragment(),
    ExamAdapter.ExamItemEvenListener, OnRvItemClickListener<Exam> {

    private val examFragmentArgs by navArgs<ExamFragmentArgs>()
    private var itemOnEditPosition: Int = -1
    private var itemOnDeletePosition: Int = -1
    private val examViewModel by inject<ExamViewModel>()
    private lateinit var examDialog: ExamDialog
    private lateinit var deleteDialog: MaterialDialog
    override val getLayoutRes: Int = R.layout.fragment_exam
    private lateinit var examAdapter: ExamAdapter


    override fun setupViews() {

    }


    override fun subscribe() {
        examAdapter = ExamAdapter()
        examAdapter.setOnRvItemClickListener(this)
        val classId = examFragmentArgs.classroom.id!!
        getExamsByClassId(classId)
        fab_fragmentExam_addNew.setOnClickListener {
            examDialog = ExamDialog(MODE_CREATE,
                onCreate = {
                    insertExam(this)
                }).apply {
                initClassroom(examFragmentArgs.classroom)
            }
            examDialog.isCancelable = false
            examDialog.show(getActivity.supportFragmentManager, null)
        }
    }

    private fun getExamsByClassId(classId: Int) {
        examViewModel.getExamsByClassId(classId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                if (it?.data != null && it.data.size > 0) {
                    examAdapter.addAll(it.data)
                    rv_fragmentExam_exams.layoutManager = LinearLayoutManager(context)
                    rv_fragmentExam_exams.adapter = examAdapter
                    examAdapter.setOnExamItemEventListener(this)
                } else {
                    // ToDo show empty state
                    getActivity.showToast(L.exam_not_exist)
                }
            }
            .doOnError {
                ExceptionMessageFactory.getMessage(it)
            }
            .doOnSubscribe {
                subscription = it
            }
            .subscribe()
    }

    private fun getExams() {

        examViewModel.getAll()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                if (it?.data != null && it.data.size > 0) {
                    examAdapter = ExamAdapter(it.data)
                    rv_fragmentExam_exams.layoutManager = LinearLayoutManager(context)
                    rv_fragmentExam_exams.adapter = examAdapter
                    examAdapter.setOnExamItemEventListener(this)
                } else {
                    // ToDo show empty state
                    getActivity.showToast(L.exam_not_exist)
                }
            }
            .doOnError {
                ExceptionMessageFactory.getMessage(it)
            }
            .doOnSubscribe {
                subscription = it
            }
            .subscribe()
    }

    fun updateExam(exam: Exam) {
        examViewModel.update(exam)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<ExamUpdateResponse>(getActivity) {
                override fun onSuccess(examUpdateResponse: ExamUpdateResponse) {
                    examAdapter.updateItem(itemOnEditPosition, examUpdateResponse.exam)
                    examDialog.dismiss()
                    EventBus.getDefault().post(OnClassroomUpdate())
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }
            })
    }

    private fun insertExam(exam: Exam) {
        DebugHelper.info(exam.examTitle)
        // Validate Exam
        examViewModel.insertExam(examFragmentArgs.classroom.id!!, exam)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleVbileObserver<Exam>(getActivity) {
                override fun onSuccess(exam: Exam) {
                    if (exam != null) {
                        examAdapter.addItem(exam)
                        examDialog.dismiss()
                        EventBus.getDefault().post(OnClassroomUpdate())
                    }
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }
            })
    }

    override fun onEdit(exam: Exam, position: Int) {
        itemOnEditPosition = position
        examDialog = ExamDialog(MODE_EDIT,
            onEdit = {
                updateExam(this)
            }).apply {
            initExam(exam)
        }
        examDialog.isCancelable = false
        examDialog.show(getActivity.supportFragmentManager, null)
    }

    override fun onDelete(exam: Exam, position: Int) {
        itemOnDeletePosition = position
        deleteDialog = MaterialDialog(
            L.exam_delete_title,
            L.exam_delete_message,
            onYes = {
                deleteExam(exam)
                deleteDialog.dismiss()
            },
            onNo = {
                deleteDialog.dismiss()
            }
        )
        deleteDialog.isCancelable = false
        deleteDialog.show(getActivity.supportFragmentManager, null)
    }

    private fun deleteExam(exam: Exam) {
        examViewModel.delete(exam)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableVbileObserver(getActivity) {
                override fun onComplete() {
                    getActivity.showToast(L.exam_deleted_successfully)
                    examAdapter.removeItem(itemOnDeletePosition)
                    EventBus.getDefault().post(OnClassroomUpdate())
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }

    override fun onPublish(exam: Exam, position: Int) {
        getActivity.showToast(exam.examEndDate)
    }

    override fun onItemClick(item: Exam, position: Int) {
        val action = ExamFragmentDirections.actionExamFragment2ToQuestionFragment(item)
        findNavController().navigate(action)
    }

}