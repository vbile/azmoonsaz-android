package ir.vbile.app.azmoonsaz.project.feature.home

import android.content.Context
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ir.vbile.app.azmoonsaz.project.model.data.auth.LoginResponse
import ir.vbile.app.azmoonsaz.project.model.data.auth.ServerResponse
import ir.vbile.app.azmoonsaz.project.model.data.auth.Token
import ir.vbile.app.azmoonsaz.project.model.data.auth.User
import ir.vbile.app.azmoonsaz.project.model.repo.user.UserRepository


class MainViewModel(private val userRepository: UserRepository) {

}