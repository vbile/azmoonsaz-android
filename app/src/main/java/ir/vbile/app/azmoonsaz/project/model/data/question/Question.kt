package ir.vbile.app.azmoonsaz.project.model.data.question

data class Question(
    val question_correct_answer: String?,
    val question_duration: String,
    val question_exam_id: Int,
    val question_is_input_mode: Int,
    val question_is_limited: Int,
    val question_title: String
)