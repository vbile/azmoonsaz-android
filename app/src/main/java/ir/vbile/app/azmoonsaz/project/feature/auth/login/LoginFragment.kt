package ir.vbile.app.azmoonsaz.project.feature.auth.login

import android.util.Patterns
import androidx.navigation.fragment.findNavController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ir.vbile.app.azmoonsaz.R
import ir.vbile.app.azmoonsaz.framework.base.BaseActivity
import ir.vbile.app.azmoonsaz.framework.base.ObserverFragment
import ir.vbile.app.azmoonsaz.framework.core.L
import ir.vbile.app.azmoonsaz.framework.handler.CompletableVbileObserver
import ir.vbile.app.azmoonsaz.framework.helper.MyPasswordTransformationMethod
import ir.vbile.app.azmoonsaz.project.feature.auth.AuthenticationViewModel
import ir.vbile.app.azmoonsaz.project.feature.auth.OnUserAuthenticate
import kotlinx.android.synthetic.main.fragment_login.*
import org.greenrobot.eventbus.EventBus
import org.koin.android.ext.android.inject


class LoginFragment : ObserverFragment() {

    private val authenticationViewModel by inject<AuthenticationViewModel>()
    override val getLayoutRes: Int get() = R.layout.fragment_login


    override fun setupViews() {

        hideBottomNavigationView()
        edt_fragmentLogin_password.transformationMethod = MyPasswordTransformationMethod()
        btn_fragmentLogin_resetPassword.setOnClickListener {
        }
        btn_fragmentLogin_login.setOnClickListener {
            val mobile = edt_fragmentLogin_mobile.text.toString().trim()
            val password = edt_fragmentLogin_password.text.toString().trim()
            val result = validateLogin(mobile, password)
            if (result) {
                loginUser(mobile, password)
            }

        }
        tv_fragmentLogin_alreadyHaveAccount.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun loginUser(mobile: String, password: String) {
        authenticationViewModel.authenticate(requireContext(), mobile, password)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(object : CompletableVbileObserver(activity as BaseActivity) {
                override fun onComplete() {
                    EventBus.getDefault().post(OnUserAuthenticate())
                    (activity as BaseActivity).showToast(L.welcome)
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

            })
    }

    private fun validateLogin(mobile: String, password: String): Boolean {
        return true
    }

    override fun subscribe() {
    }

    private fun isValidMobile(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }




}