package ir.vbile.app.azmoonsaz.project.model.data.exam.one


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam

@SuppressLint("ParcelCreator")
@Parcelize
data class ExamUpdateResponse(
    @SerializedName("data")
    var `exam`: Exam
) : Parcelable