package ir.vbile.app.azmoonsaz.framework.config.lang

class FaLang : Lang() {
    override val hello_world = "سلام"
    override val permission_required_message = "دسترسی لازم است لطفاً آن را بدهید"
    override val button_ok = "باشه"
    override val button_cancel = "نمیخوام"
    override val permission_required_title = "درخواست مجوز"
    override val notification_title = "سلام این "
    override val notification_text = "نوتیفیکشن"
    override val notification_id = "آی دی نوتیفیکیشن"
    override val button_yes = "بله"
    override val button_no = "نه"
    override val appName = "رینانو"
    override val version = "نسخه: "
    override val menu_about = "درباره ماه"
    override val menu_feedback = "ارسال گزارش"
    override val menu_preferences = "تنظیمات"
    override val menu_products = "محصولات"
    override val menu_application_category = "نرم افزار"
    override val menu_about_application = "امکانات"

    override val check_your_phone_number: String = "لطفاً شماره موبایل خود را وارد کنید"
    override val check_your_password: String = "لطفاً رمز عبور خود را وارد کنید"
    override val wrong_phone_number: String = "شماره موبایل با ۰۹ شروع می شود"
    override val wrong_password: String = "رمز عبور وارد شده صحصح نیست"
    override val you_are_not_register_yet: String =
        "شما پیش از این ثبت نام نکرده اید لطفاً ثبت نام کنید"
    override val unauthorized: String = "شما دسترسی لازم را ندارید"
    override val welcome: String = "خوش آمدید"
    override val network_connection_failed: String = "عدم اتصال به اینترنت"
    override val your_account_is_not_active: String = "حساب کاربری شما فعال نیست"


    // Exam Lang Resource
    override val exam_not_found: String = "این کلاس آزمونی ندارد"
    override fun exam_count(count: Int): String = "آزمون\u200Cها:$count"
    override fun getTimes(): List<String> {
        val times = mutableListOf<String>()
        for (i in 1 until 59) {
            times.add("$i دقیقه")
        }
        return times
    }

    val statusFaArray = listOf("پیش نویس", "در صف انتشار", "منتشر شده")
    override fun getExamStatus(): List<String> {
        return statusFaArray
    }


    override fun getExamsStudentsCount(studentsCount: Int?): String {
        return "تعداد شرکت کنندگان: $studentsCount"
    }

    override fun getExamStartDate(date: String): String {
        return "تاریخ شروع: $date"
    }

    override fun getExamEndDate(date: String): String {
        return "تاریخ پایان: $date"
    }

    override fun getExamDuration(examDuration: String): String {
        return "مدت زمان: $examDuration"
    }

    override val exam_edit: String = "ویرایش آزمون"

    override var exam_delete_message: String = "آیا از حذف آزمون مطمئن هستید؟"
    override var exam_delete_title: String = "حذف آزمون"
    override var exam_deleted_successfully: String = "آزمون با موفقیت حذف شد"
    override var exam_not_exist: String = "آزمونی یافت نشد"
    override var exam_end_date: String = "تاریخ پایان:"
    override var exam_start_date: String = "تاریخ شروع:"

    override var classroom_delete_message: String = "آیا از حذف کلاس مطمئن هستید؟"
    override var classroom_deleted_successfully: String = "کلاس شما با موفقیت حذف شد"
    override var classroom_delete_title: String = "حذف کلاس"
    override var classroom_not_exist: String = "آزمونی موجود نمی باشد"
    override var classroom_edit: String = "ویرایش کلاس"
    override var classroom_create: String = "ایجاد کلاس"
}