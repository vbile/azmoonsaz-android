package ir.vbile.app.azmoonsaz.project.feature.auth

object TokenContainer {
    var token: String = ""

    fun updateToken(token: String?) {
        if (token != null) {
            TokenContainer.token = token
        }
    }

}