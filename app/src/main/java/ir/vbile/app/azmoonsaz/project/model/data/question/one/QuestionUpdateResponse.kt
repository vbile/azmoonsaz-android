package ir.vbile.app.azmoonsaz.project.model.data.question.one


import com.google.gson.annotations.SerializedName
import ir.vbile.app.azmoonsaz.project.model.data.question.Question


data class QuestionUpdateResponse(
    @SerializedName("data")
    val `question`: Question
)