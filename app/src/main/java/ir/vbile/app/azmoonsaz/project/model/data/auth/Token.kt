package ir.vbile.app.azmoonsaz.project.model.data.auth


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Token(
    @SerializedName("access_token")
    var accessToken: String,
    @SerializedName("expires_in")
    var expiresIn: Int,
    @SerializedName("refresh_token")
    var refreshToken: String,
    @SerializedName("token_type")
    var tokenType: String
) : Parcelable