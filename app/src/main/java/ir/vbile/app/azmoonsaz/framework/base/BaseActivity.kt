package ir.vbile.app.azmoonsaz.framework.base

import android.content.res.Configuration
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import ir.vbile.app.azmoonsaz.framework.core.App
import ir.vbile.app.azmoonsaz.framework.handler.PermissionHandler
import ir.vbile.app.azmoonsaz.framework.helper.DebugHelper
import ir.vbile.app.azmoonsaz.framework.helper.ThemeHelper
import ir.vbile.app.azmoonsaz.project.core.Config
import org.greenrobot.eventbus.EventBus
import java.util.*


abstract class BaseActivity : AppCompatActivity() {
    private val permissionHandlers = mutableListOf<PermissionHandler>()
    abstract fun getCoordinatorLayout(): Int
    override fun onCreate(savedInstanceState: Bundle?) {
        initializeConfigurator()
        super.onCreate(savedInstanceState)
        App.currentActivity = this
        ThemeHelper.applyTheme()
        App.context = applicationContext
        App.setLocate("fa")
    }




    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        for (permissionHandler in permissionHandlers) {
            val handled =
                permissionHandler.processOnPermissionResult(requestCode, permissions, grantResults)
            if (handled) {
                return
            }
        }
    }

    fun showSnackBar(message: String) {
        Snackbar.make(findViewById(getCoordinatorLayout()), message, Snackbar.LENGTH_LONG).show()
    }

    fun addPermissionHandler(permissionHandler: PermissionHandler) {
        permissionHandlers.add(permissionHandler)
    }

    private fun initializeConfigurator() {
        if (App.config == null) {
            App.config = Config()
        }
    }

    fun showToast(message: Any) {
        Toast.makeText(applicationContext, "" + message, Toast.LENGTH_LONG).show()
    }

    fun showToast(message: Any, length: Int) {
        Toast.makeText(applicationContext, "" + message, length).show()
    }

    override fun onResume() {
        super.onResume()
        App.currentActivity = this
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        DebugHelper.warn("Activity ${this::class.java.simpleName} Started ")
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        App.setLocate("fa")
    }

}