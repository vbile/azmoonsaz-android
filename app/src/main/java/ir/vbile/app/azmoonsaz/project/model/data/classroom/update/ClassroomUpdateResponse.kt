package ir.vbile.app.azmoonsaz.project.model.data.classroom.update


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import io.reactivex.SingleOnSubscribe
import ir.vbile.app.azmoonsaz.project.model.data.classroom.Classroom

@SuppressLint("ParcelCreator")
@Parcelize
data class ClassroomUpdateResponse(
    @SerializedName("data")
    var `data`: Classroom
) : Parcelable