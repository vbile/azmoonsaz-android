package ir.vbile.app.azmoonsaz.project.model.repo.exam

import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ir.vbile.app.azmoonsaz.project.model.data.exam.Exam
import ir.vbile.app.azmoonsaz.project.model.data.exam.ExamResponse
import ir.vbile.app.azmoonsaz.project.model.data.exam.one.ExamUpdateResponse

class ExamRepository(private val examCloudDataSource: ExamCloudDataSource) {
    fun get(id: Int): Single<ExamResponse> {
        TODO("Not yet implemented")
    }

    fun delete(exam: Exam): Completable {
        return examCloudDataSource.delete(exam)
    }

    fun getAll(page: Int): Single<List<ExamResponse>> {
        TODO("Not yet implemented")
    }

    fun insert(classroomId: Int, jsonObject: JsonObject): Single<Exam> {
        return  examCloudDataSource.insert(classroomId,jsonObject)
    }

    fun update(classroomId: Int, examId: Int, jsonObject: JsonObject): Single<ExamUpdateResponse> {
        return examCloudDataSource.update(classroomId, examId, jsonObject)
    }

    fun getAllExams(): Flowable<ExamResponse> {
        return examCloudDataSource.getAllExams()
    }

    fun getExamsByClassId(classId: Int): Flowable<ExamResponse> {
        return examCloudDataSource.getExamsByClassId(classId)
    }


}